package jp.co.jmas.ForkLift.activity;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.orm.SugarContext;

import jp.co.jmas.ForkLift.classes.DataLogRecorder;
import jp.co.jmas.ForkLift.classes.GlobalConfig;
import jp.co.jmas.ForkLift.model.ApplicationRecordModel;
import jp.co.jmas.ForkLift.service.ForkLiftService;
import jp.co.jmas.ForkLift.util.ScreenBrightnessNotifier;


/**
 * Created by Dell on 8/31/2016.
 */
public class BaseActivity extends AppCompatActivity {

    // declaring objects and variables
    PowerManager powerManager;
    PowerManager.WakeLock wakeLock;
    SharedPreferences savedSharedPreference;
    SharedPreferences.Editor editor;
    ScreenBrightnessNotifier screenBrightnessNotifier;
    Context mContext;
    boolean exit;
    ApplicationRecordModel applicationRecordModel;


    /**
     * declaring onCreate() method
     *
     * @param savedInstanceState
     * @param
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("test_copy","forklift-copy");
        applicationRecordModel = new ApplicationRecordModel();
        mContext = BaseActivity.this;
        SugarContext.init(mContext);
        exit = false;

        savedSharedPreference = getSharedPreferences("forklift", MODE_PRIVATE);
        editor = savedSharedPreference.edit();


        powerManager = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        screenBrightnessNotifier = new ScreenBrightnessNotifier(mContext);

        boolean screen_brightness = savedSharedPreference.getBoolean("backlight_dim", false);
        screenBrightnessNotifier.dim_brightness(screen_brightness);




    }


    /**
     * handling backpress event method for all the activity
     */
    @Override
    public void onBackPressed() {

        if (exit) {


            //Set Sdk Authorized True
            editor.putBoolean("sdkAuthorized", false);
            editor.commit();

            //Stop Service when exit from application
            stopService(new Intent(this, ForkLiftService.class));

            if(GlobalConfig.isDebug)
            {
                Log.e("Service",""+"Stop");


            }
            //End stop service....

            // setting forkno  to null value
            DataLogRecorder.setForkno("");



            // setting new intent
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);

            // setting flags to clear activity
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


            // calling finish method for current activity
            finish();

            //going to home screen
            startActivity(a);
        }

        else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }


    /**
     * declaring the onDestroy() method for BaseActivity.java
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //wakelock will be released by default on onDestroy()
        if (wakeLock.isHeld()) {
            wakeLock.release();
        }
    }

    /**
     * testing database save method
     */


}
