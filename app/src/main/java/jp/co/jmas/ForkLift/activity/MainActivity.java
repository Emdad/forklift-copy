package jp.co.jmas.ForkLift.activity;


import android.app.ActivityManager;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import java.util.Locale;

import jp.co.jmas.ForkLift.R;
import jp.co.jmas.ForkLift.beaconSDK.SDKState;
import jp.co.jmas.ForkLift.beaconSDK.SDKTaskManager;
import jp.co.jmas.ForkLift.broadcasts.BatteryStatusReceiver;
import jp.co.jmas.ForkLift.classes.BetteryManagersHelper;
import jp.co.jmas.ForkLift.classes.DataLogRecorder;
import jp.co.jmas.ForkLift.classes.GlobalConfig;
import jp.co.jmas.ForkLift.classes.ManageCustomLog;
import jp.co.jmas.ForkLift.fragments.InitialScreenFragment;
import jp.co.jmas.ForkLift.fragments.StartWorkScreenFragment;
import jp.co.jmas.ForkLift.fragments.WorkingScreenFragment;
import jp.co.jmas.ForkLift.service.ForkLiftService;

public class MainActivity extends BaseActivity implements InitialScreenFragment.onSdkInitializeListener,
        StartWorkScreenFragment.onUsernoFragmentListener,
        StartWorkScreenFragment.onUsernoOnGoBackListener,
        WorkingScreenFragment.onCargoSwitchListener,
        WorkingScreenFragment.cargoSwitchOffOnListener {


    private BluetoothAdapter BA;
    private final String TAG_LANG = "ja"; //example en = english, ja= japan;

    SDKTaskManager sdkTaskManager;
    ManageCustomLog manageCustomLog;

    SharedPreferences savedSharedPref;
    SharedPreferences.Editor editor;
    Toolbar toolbar;


    BatteryStatusReceiver batteryStatusReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        //Change Language
        changeLanguage(TAG_LANG);


        savedSharedPref = getSharedPreferences("forklift", MODE_PRIVATE);
        editor = savedSharedPref.edit();


        BA = BluetoothAdapter.getDefaultAdapter();
        sdkTaskManager = new SDKTaskManager(MainActivity.this);
        manageCustomLog = new ManageCustomLog();


        //Monitoring the Battery Level and Charging State
        monitorBatteryChargingChecker();


        //Check Forkno already set, and beaconsdk run successfully
        //First time forkno initial screen will be display, Second time  start work screen display, if sdk run successfully
        String forkno = DataLogRecorder.getForkno();


        if (GlobalConfig.isDebug) {
            Log.d(GlobalConfig.TAG, "InitialScreen Forkno = " + forkno);

        }

        //Check Forkno is entered before
        if (forkno.equalsIgnoreCase("")) {
            //called initialScreen fragment
            initialScreen();
        } else {
            //called startWorkScreen fragment
            startWorkScreen();
        }

    }


    /**
     * This Method called from StartWrokScreenFragment
     *
     * @param flag
     */

    @Override
    public void onCalledSettingsForknofragment(boolean flag) {

        initialScreen();

    }


    /**
     * Load InitialScreenFragment to Mainactivity Container.
     */
    public void initialScreen() {
        //Set TItle and Menubar
        toolbar.setTitle(R.string.title_header_Initial_screen);
        setSupportActionBar(toolbar);


        android.app.FragmentManager fm = getFragmentManager();
        android.app.FragmentTransaction ft = fm.beginTransaction();
        InitialScreenFragment initialScreenFragment = new InitialScreenFragment();
        ft.replace(R.id.fragmentContainer, initialScreenFragment).addToBackStack("InitialScreenFragment");
        ft.commit();
    }

    //Called from multiple place ,(When sdk initilize success and when workend ) this is callback function
    public void toStartWorkScreen() {
        startWorkScreen();
    }


    public void startWorkScreen() {

        //Set TItle and Menubar
        toolbar.setTitle(R.string.tv_title_header);
        setSupportActionBar(toolbar);

        android.app.FragmentManager fm = getFragmentManager();
        android.app.FragmentTransaction ft = fm.beginTransaction();
        StartWorkScreenFragment usernoDeptNoFragment = new StartWorkScreenFragment();
        ft.replace(R.id.fragmentContainer, usernoDeptNoFragment).addToBackStack("StartWorkScreenFragment");
        ft.commit();
    }

    /**
     * sdkInitializeCall Callback function called from Initial Screen Fragment
     *
     * @param getForkno
     */
    @Override
    public void sdkInitializeCall(final String getForkno) {

        sdkTaskManager.sdkInitilizeSettings(new SDKTaskManager.SdkStatusListener() {

            @Override
            public void sdkManagerCallBack(SDKState sdkStateObj) {

                if (sdkStateObj.isStatus()) {

                    if (GlobalConfig.isDebug) {
                        Log.e(GlobalConfig.TAG, "SDK Initial Success =" + sdkStateObj.getMessage());
                    }

                    //Called update sdk.
                    sdkTaskManager.startUpdateSdk();

                    //Set Sdk Authorized status, after successfully initialize sdk
                    editor.putBoolean("sdkAuthorized", true);
                    editor.commit();

                    DataLogRecorder.setSdkAuthorized(true);

                    //Delete Database Old Record
                    manageCustomLog.deleteModelData();

                    //Set Forklift Log
                    DataLogRecorder.setForkno(getForkno);
                    DataLogRecorder.setUserno("");
                    DataLogRecorder.setDeviceID(DataLogRecorder.getDeviceID());
                    DataLogRecorder.setWorkingflg(0);
                    DataLogRecorder.setTsuminiflg(2);
                    DataLogRecorder.setWorkingSequence(GlobalConfig.getWorkingSequenceDate());
                    DataLogRecorder.setBatteryLevel(BetteryManagersHelper.getBatteryLevel(MainActivity.this));

                    /**
                     * Get Console Debug Log
                     */

                    if (GlobalConfig.isDebug) {

                        Log.e(GlobalConfig.TAG, "BeaconLogging: SDK Initilized Set Fork no");
                        Log.e(GlobalConfig.TAG, "BeaconLogging: forkno =" + DataLogRecorder.getForkno());
                        Log.e(GlobalConfig.TAG, "BeaconLogging: userno =" + DataLogRecorder.getUserno());
                        Log.e(GlobalConfig.TAG, "BeaconLogging: deviceID =" + DataLogRecorder.getDeviceID());
                        Log.e(GlobalConfig.TAG, "BeaconLogging: workingflg =" + DataLogRecorder.getWorkingflg());
                        Log.e(GlobalConfig.TAG, "BeaconLogging: Tsuminiflg =" + DataLogRecorder.getTsuminiflg());
                        Log.e(GlobalConfig.TAG, "BeaconLogging: workingSequence =" + DataLogRecorder.getWorkingSequence());
                        Log.e(GlobalConfig.TAG, "BeaconLogging: batteryLevel =" + DataLogRecorder.getBatteryLevel());
                    }

                    /**
                     * End Get Console Debug Log
                     */

                    //Check service
                    ServiceStartControl();


                    //Go to StartWORK SCREEN
                    toStartWorkScreen();


                } else {


                    if (GlobalConfig.isDebug) {
                        Log.e(GlobalConfig.TAG, "state = " + sdkStateObj.isStatus());
                        Log.e(GlobalConfig.TAG, "message =" + sdkStateObj.getMessage());
                    }

                    String title = getResources().getString(R.string.sdkInitializeAlertErrorTitle);
                    String sdkErrorMessage = getResources().getString(R.string.sdkInitializeAlertErrorMessage);

                    setAlertDialog(title, sdkErrorMessage);
                }
            }
        });

    }

    /**
     * startWork Method call from StartWorkScreenFragment start button event.
     */
    @Override
    public void startWork(boolean flag) {

        // saving data CustomLog/workStartEnd() in the database table
        manageCustomLog.workStartEnd();

        //Set KadoRecordCSV to DatalogRecords Array and it will be getting data from service
        DataLogRecorder.csvStringArray.push(manageCustomLog.getKadoRecordCSV());


        //Set title action bar
        toolbar.setTitle(R.string.title_header_working_screen);
        setSupportActionBar(toolbar);

        android.app.FragmentManager fm = getFragmentManager();
        android.app.FragmentTransaction ft = fm.beginTransaction();
        WorkingScreenFragment workingScreenFragment = new WorkingScreenFragment();
        ft.replace(R.id.fragmentContainer, workingScreenFragment).addToBackStack("WorkingScreenFragment");
        ft.commit();
    }


    /**
     * cargoswitchOffOn Method call from WorkingScreenFragment end button event.
     */
    @Override
    public void cargoswitchOffOn() {

        //saving data CustomLog/cargoSituation() in the database table
        manageCustomLog.cargoSituation();

        //Set TsuminiRecordCSV to DatalogRecords Array and it will be getting data from service
        DataLogRecorder.csvStringArray.push(manageCustomLog.getTsuminiRecordCSV());
    }

    /**
     * Endwork Method call from WorkingScreenFragment end button event.
     *
     * @param flag
     */

    @Override
    public void endWork(boolean flag) {

        // saving data CustomLog/workStartEnd() in the database table
        manageCustomLog.workStartEnd();

        DataLogRecorder.csvStringArray.push(manageCustomLog.getKadoRecordCSV());

        //Go to Startwork Screen.
        toStartWorkScreen();

    }


    /**
     * Common Alert Dialog
     *
     * @param title
     * @param message
     */
    public void setAlertDialog(String title, String message) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }


    /**
     * Language change Method
     *
     * @param currentLang
     */
    public void changeLanguage(String currentLang) {
        Locale locale = new Locale(currentLang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

    }


    /**
     * Check if Service is running
     *
     * @param ForkLiftService
     * @return true if one or more service is running in background thread
     */
    private boolean isServiceRunning(Class<?> ForkLiftService) {
        ActivityManager manager = (ActivityManager) getApplicationContext().getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (ForkLiftService.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    /**
     * ServiceStartControl check the service already  running or not.
     */
    public void ServiceStartControl() {

        //Service Called for send data to server and local database
        if (isServiceRunning(ForkLiftService.class)) {
            //Service Already Started no need again start service

            if (GlobalConfig.isDebug) {
                Log.d(GlobalConfig.TAG, "Service Already Started");
            }

        } else {

            Intent intent = new Intent(MainActivity.this, ForkLiftService.class);
            startService(intent);

            if (GlobalConfig.isDebug) {

                Log.d(GlobalConfig.TAG, "Start New Service");
            }
        }

        //End service Called.
    }


    /**
     * monitorBatteryChargingChecker
     * Register BatteryReceiver , Monitoring the Battery Level and Charging State
     */
    public void monitorBatteryChargingChecker() {


        //broadcast receiver check and enable call method
        if (savedSharedPref.getBoolean("monitor_battery_charging", false)) {

            batteryStatusReceiver = new BatteryStatusReceiver();

            if (GlobalConfig.isDebug) {
                Log.d(GlobalConfig.TAG, "isMonitorCharging =" + savedSharedPref.getBoolean("monitor_battery_charging", false));
            }

            // Register receiver that handles screen on and screen off logic
            IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            filter.addAction(Intent.ACTION_BATTERY_LOW);
            filter.addAction(Intent.ACTION_BATTERY_OKAY);
            filter.addAction(Intent.ACTION_POWER_CONNECTED);
            filter.addAction(Intent.ACTION_POWER_DISCONNECTED);


            this.registerReceiver((batteryStatusReceiver),
                    new IntentFilter(filter)
            );


        }

    }

    @Override
    protected void onStop() {

        if (batteryStatusReceiver != null) {
            this.unregisterReceiver(batteryStatusReceiver);
            batteryStatusReceiver = null;
        }


        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (GlobalConfig.isDebug) {
            Log.d(GlobalConfig.TAG, "MainActivity onDestory");
        }

        super.onDestroy();

    }


}

