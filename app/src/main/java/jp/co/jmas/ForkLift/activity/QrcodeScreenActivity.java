package jp.co.jmas.ForkLift.activity;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import jp.co.jmas.ForkLift.R;
import jp.co.jmas.ForkLift.classes.GlobalConfig;


public class QrcodeScreenActivity extends AppCompatActivity {

    // declaring the instance objects and variables;

    Toolbar toolbar;

    String qrCodeType = null;


    private Camera mCamera;
    private CameraPreview mPreview;
    private Handler autoFocusHandler;

    ImageScanner scanner;
    FrameLayout preview;

    private boolean barcodeScanned = false;
    private boolean previewing = true;


    SharedPreferences sharedpreferences;
    MenuItem removeSettings, cancelMenu, confirm_menu;
    public static final String MyPREFERENCES = "forklift" ;



    /**
     * declaring constructor with parameter
     *
     * @param context
     */
    public QrcodeScreenActivity(Context context) {
        context = getApplicationContext();
    }


    /**
     * declaring constructor without parameter
     */
    public QrcodeScreenActivity() {

    }


    /**
     * declaring onCreate() method
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_qrcodescreen);

        sharedpreferences = this.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        toolbar = (Toolbar)findViewById(R.id.toolbar);

        toolbar.setTitle(R.string.title_qr_code_reader);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        this.qrCodeType = intent.getStringExtra("qrCodeType");






        autoFocusHandler = new Handler();
        mCamera = getCameraInstance();

        // Instance barcode scanner
        scanner = new ImageScanner();
        scanner.setConfig(0, Config.X_DENSITY, 3);
        scanner.setConfig(0, Config.Y_DENSITY, 3);

        mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);
        preview = (FrameLayout) findViewById(R.id.cameraPreview);

        preview.addView(mPreview);
        if (barcodeScanned) {
            barcodeScanned = false;
            mCamera.setPreviewCallback(previewCb);
            mCamera.startPreview();
            previewing = true;
            mCamera.autoFocus(autoFocusCB);
        }
    }

    /**
     * declaring onPause() method
     */
    public void onPause() {
        super.onPause();
        releaseCamera();
    }

    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
        }
        return c;
    }

    private void releaseCamera() {
        if (mCamera != null) {
            previewing = false;


            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;


            mPreview.getHolder().removeCallback(mPreview);
        }


    }

    /**
     * new runnable object implementation
     */
    private Runnable doAutoFocus = new Runnable() {
        public void run() {
            if (previewing)
                mCamera.autoFocus(autoFocusCB);
        }
    };

    Camera.PreviewCallback previewCb = new Camera.PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera camera) {
            Camera.Parameters parameters = camera.getParameters();
            Camera.Size size = parameters.getPreviewSize();

            Image barcode = new Image(size.width, size.height, "Y800");
            barcode.setData(data);

            int result = scanner.scanImage(barcode);

            if (result != 0) {
                previewing = false;
                mCamera.setPreviewCallback(null);
                mCamera.stopPreview();

                SymbolSet syms = scanner.getResults();

                boolean barcodefound = false;
                String qrcodeResultData = "";
                for (Symbol sym : syms) {

                    qrcodeResultData = sym.getData();
                    barcodefound = true;
                }

                if (barcodefound) {
                    SharedPreferences.Editor editor = sharedpreferences.edit();


                    if(GlobalConfig.isDebug){
                        Log.e(GlobalConfig.TAG,"QR Code Data = "+qrcodeResultData);

                    }



                    if(qrCodeType.equals("requestToken"))
                    {
                        editor.putString("temp_QrCodeForRequestToken", qrcodeResultData);

                        if(GlobalConfig.isDebug){
                            Log.e(GlobalConfig.TAG,"Scan RequestToken QR Code = "+qrcodeResultData);

                        }
                    }
                    else if(qrCodeType.equals("secretKey"))
                    {
                        editor.putString("temp_QrCodeForSecretKey", qrcodeResultData);

                        if(GlobalConfig.isDebug){
                            Log.e(GlobalConfig.TAG,"Scan SecretKey QR Code = "+qrcodeResultData);

                        }
                    }
                    else
                    {

                    }

                    editor.commit();


                    Intent i = new Intent(QrcodeScreenActivity.this, SettingsActivity.class);
                    i.putExtra("FromCameraActivity", "camera");
                    startActivity(i);

                }
            }
        }
    };

    // Mimic continuous auto-focusing
    Camera.AutoFocusCallback autoFocusCB = new Camera.AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            autoFocusHandler.postDelayed(doAutoFocus, 1000);
        }
    };




    /**
     * adding the options menu items and onClick Listener
     * @param menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        removeSettings = menu.findItem(R.id.action_settings);
        cancelMenu = menu.findItem(R.id.action_cancel);
        confirm_menu = menu.findItem(R.id.confirm_menu);
        confirm_menu.setVisible(false);
        cancelMenu .setVisible(false);
        removeSettings.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {



                Intent intent = new Intent(QrcodeScreenActivity.this, SettingsActivity.class);
                startActivity(intent);


                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);

    }
}