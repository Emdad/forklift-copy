package jp.co.jmas.ForkLift.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import android.support.v7.widget.Toolbar;

import java.util.Locale;

import jp.co.jmas.ForkLift.fragments.SettingsFragment;
import jp.co.jmas.ForkLift.R;
import jp.co.jmas.ForkLift.fragments.UsernoDeptnoSettingScreenFragment;


public class SettingsActivity extends BaseActivity implements SettingsFragment.onReadQrcodeListener,SettingsFragment.deptnoFragmentListener {


    Toolbar toolbar;

    private final String TAG_LANG = "ja"; //example en = english, ja= japan;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        //Change Language(Load Default Japan Language)
        changeLanguage(TAG_LANG);




        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_setting);
        setSupportActionBar(toolbar);
        android.app.FragmentManager fm = getFragmentManager();
        android.app.FragmentTransaction ft = fm.beginTransaction();
        SettingsFragment settingsFragment = new SettingsFragment();
        ft.add(R.id.settingsFragContainer, settingsFragment).addToBackStack("SettingsFragment");
        ft.commit();



    }


    @Override
    public void calledQrcodeFragment(String type) {

        Intent i = new Intent(SettingsActivity.this, QrcodeScreenActivity.class);
        i.putExtra("qrCodeType", type);
        startActivity(i);

    }


    @Override
    public void calleddeptnoFragment() {

        android.app.FragmentManager fm = getFragmentManager();
        android.app.FragmentTransaction ft = fm.beginTransaction();
        UsernoDeptnoSettingScreenFragment usernoDeptnoSettingScreenFragment = new UsernoDeptnoSettingScreenFragment();
        ft.replace(R.id.settingsFragContainer, usernoDeptnoSettingScreenFragment).addToBackStack("UsernoDeptnoSettingScreenFragment");
        ft.commit();

    }

    public void changeLanguage(String currentLang)
    {

        //currentLang = en,ja,fr

        Locale locale = new Locale(currentLang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());



    }

}
