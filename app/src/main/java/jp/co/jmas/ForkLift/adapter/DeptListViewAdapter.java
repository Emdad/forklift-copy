package jp.co.jmas.ForkLift.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import jp.co.jmas.ForkLift.R;

/**
 * ListView adapter for list view in fragment_userno_deptno_setting layout
 */

public class DeptListViewAdapter extends BaseAdapter {


    // declaring objects and variables
    Context context;
    String[] data;
    TextView tv_item_name, tv_item_count;
    private static LayoutInflater inflater = null;


    /**
     * implementing constructor with parameters for DeptListViewAdapter
     * @return
     */
   public DeptListViewAdapter(Context context, String[] data) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = data;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    /**
     * implementing getcount() method for adapter
     * @return
     */
    @Override
    public int getCount() {
        return data.length;
    }


    /**
     * implementing getItem() method for adapter
     * @return
     */
    @Override
    public Object getItem(int i) {
         return data[i];
    }



    /**
     * implementing getItemId() method for adapter
     * @return
     */
    @Override
    public long getItemId(int i) {
        return i;
    }


    /**
     * implementing getView() method for adapter
     * @return
     */
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;

        if (v == null)
                {
                    v = inflater.inflate(R.layout.custom_row_registration_dept_no,null);
                    tv_item_name = (TextView)v.findViewById(R.id.lv_custom_row_name);
                    tv_item_count =(TextView)v.findViewById(R.id.tv_registration_number_counter);

                    tv_item_name.setText(data[i]);
                    tv_item_count.setText(String.valueOf(i+1));

                }
        return v;
    }
}
