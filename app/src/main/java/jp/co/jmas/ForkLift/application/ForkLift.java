package jp.co.jmas.ForkLift.application;

import android.app.Application;

import com.orm.SugarContext;

/**
 * Created by Dell on 10/17/2016.
 */

/**
 * declaring the
 */
public class ForkLift extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // initializing the Application context for the SugarContext
        SugarContext.init((getApplicationContext()));
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        // terminating the Application context for the SugarContext
        SugarContext.terminate();
    }


}
