package jp.co.jmas.ForkLift.beaconSDK;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;


import com.beacapp.BeaconEventListener;
import com.beacapp.FireEventListener;
import com.beacapp.JBCPException;
import com.beacapp.JBCPManager;
import com.beacapp.ShouldUpdateEventsListener;
import com.beacapp.UpdateEventsListener;
import com.beacapp.service.BeaconEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.Map;

import jp.co.jmas.ForkLift.classes.ManageCustomLog;
import jp.co.jmas.ForkLift.classes.DataLogRecorder;
import jp.co.jmas.ForkLift.classes.GlobalConfig;


/**
 * Created by Dell on 9/20/2016.
 */
public class SDKTaskManager {


//      declaring the variables and objects

    Context context;

    //SDK INITIALIZE Variable

    String requestToken, secretKey;
    private JBCPManager jbcpManager;
    ManageCustomLog manageCustomLog;

    Thread thread = null;

    boolean sdkInitialStatus = true;
    String sdkInitialErrorMessage = "";


    SharedPreferences savedSharedPref;


    //Define Interface for SDK State

    public interface SdkStatusListener {
        public abstract void sdkManagerCallBack(SDKState sdkStateObj);
    }


    //End Define interface...


    public SDKTaskManager(Context context) {

        savedSharedPref = context.getSharedPreferences("forklift", context.MODE_PRIVATE);
        this.context = context;
        this.requestToken = savedSharedPref.getString("QrCodeForRequestToken", "");
        this.secretKey = savedSharedPref.getString("QrCodeForSecretKey", "");

        if(GlobalConfig.isDebug)
        {
            Log.d("Activationkey",""+requestToken);
            Log.d("SecretKey",""+secretKey);
        }

    }


    /**
     * @return
     */
    public JBCPManager sdkInitilizeSettings() {
        return sdkInitilizeSettings(new SDKTaskManager.SdkStatusListener() {

            @Override
            public void sdkManagerCallBack(SDKState sdkStateObj) {
            }
        });
    }

    /**
     * SDK Initialize check request token and secret key is presence
     *
     * @param sdkStatusListener
     */
    public JBCPManager sdkInitilizeSettings(SdkStatusListener sdkStatusListener) {


        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    jbcpManager = JBCPManager.getManager(context,
                            requestToken,
                            secretKey,
                            null);
                } catch (JBCPException e) {
                    Log.e("Error:", ":Message : " + e.getMessage() + ",Code: " + e.getCode());
                    sdkInitialStatus = false;
                    sdkInitialErrorMessage = e.getMessage() + ",Code: " + e.getCode();

                    return;
                }

                if (jbcpManager == null) {
                    sdkInitialStatus = false;
                    sdkInitialErrorMessage = "";

                    return;
                }
            }
        });

        thread.start();

        try {
            thread.join();
        } catch (InterruptedException ie) {
            // TODO
        }

        Log.e("sdkInitialStatus", ":" + sdkInitialStatus);

        // loadingDialog.dismiss();


        SDKState sdkState = new SDKState();
        sdkState.setStatus(sdkInitialStatus);
        sdkState.setMessage(sdkInitialErrorMessage);

//              SDK CALL BACK METHOD
        sdkStatusListener.sdkManagerCallBack(sdkState);


        return jbcpManager;

    }


    /**
     * SDK Initialize when start button click
     */
    public void startUpdateSdk() {


        //UpdateEvents Called
        jbcpManager.setUpdateEventsListener(updateEventsListener);
        jbcpManager.setShouldUpdateEventsListener(shouldUpdateEventsListener);
        jbcpManager.setFireEventListener(fireEventListener);


        // To update the event
        jbcpManager.startUpdateEvents();


        //JBCPManager.SCAN_MODE = 2;
        jbcpManager.setBeaconEventListener(beaconEventListener);

        // Start Scan
        jbcpManager.startScan();


        //Set Device id to Log class
        String deviceID = jbcpManager.getDeviceIdentifier();
        DataLogRecorder.setDeviceID(deviceID);

        if(GlobalConfig.isDebug)
        {
            Log.e("Device ID", "From BecappSDK:" + deviceID);
        }



    }


    /**
     * SDK Initialize During Update
     */
    public void updateSdk() {
        //Called SDK Initialize Method

        jbcpManager.setUpdateEventsListener(updateEventsListener);
        jbcpManager.setShouldUpdateEventsListener(shouldUpdateEventsListener);
        jbcpManager.setFireEventListener(fireEventListener);

        // To update the event
        jbcpManager.startUpdateEvents();


    }

    /**
     * SDK Custom Log CSV Send
     */
    public void sendCSVDataToCustomLog(String csvMessage) {


        if (GlobalConfig.isDebug) {

            // byte[] decodeValue = Base64.decode(csvMessage, Base64.DEFAULT);
            //Log.e("Customlog Decode Data", " CSV String = \n" + new String(decodeValue));
            //Log.e("Custom Logo", ": base64EncodeCSVString :\n" + csvMessage);

        }

        try{

        jbcpManager.customLog(csvMessage);

        } catch (Exception e){
            e.printStackTrace();
        }


    }


    /**
     * Sdk Stop Scan Method
     */
    public void sdkStopScan() {
        // StopScan
        jbcpManager.stopScan();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("確認");
        alertDialogBuilder.setMessage("終了処理に成功しました。");

        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,
                                        int which) {


                        dialog.dismiss();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }


    /**
     * Decide should update listener or not
     */
    private ShouldUpdateEventsListener shouldUpdateEventsListener = new ShouldUpdateEventsListener() {
        @Override
        public boolean shouldUpdate(Map<String, Object> map) {
            return true;
        }
    };

    /**
     * Update event listener
     */
    private UpdateEventsListener updateEventsListener = new UpdateEventsListener() {
        @Override
        public void onProgress(int i, int i1) {
            // TODO

        }


        @Override
        public void onFinished(JBCPException e) {
            if (e != null) {

                // Start Scan
                jbcpManager.startScan();


            }
        }
    };

    /**
     * Generate The Event listener
     */
    private FireEventListener fireEventListener = new FireEventListener() {
        @Override
        public void fireEvent(JSONObject jsonObject) {



            manageCustomLog = new ManageCustomLog();

            String event_id = "";
            String date_time = "";
            String event_type = "";
            String event_details = "";

            JSONObject triggersArrayObj = null;
            String uuid = "";
            int major = 0;
            int minor = 0;
            String signal_conditions = "";
            String region_condition = "";
            int priority = 0;

            try {
                triggersArrayObj = jsonObject.optJSONArray("triggers").getJSONObject(0);
                //Log.e("triggers", ":" + triggersArrayObj);
                signal_conditions = triggersArrayObj.getString("signal_condition");
                region_condition = triggersArrayObj.getString("region_condition");

                JSONObject beaconObj = triggersArrayObj.getJSONArray("beacons").getJSONObject(0);
                //Log.e("beaconObj", ":" + beaconObj);


                uuid = beaconObj.getString("uuid");
                major = beaconObj.getInt("major");
                minor = beaconObj.getInt("minor");


                if (signal_conditions.equals("immediate")) {
                    priority = 1;
                } else if (signal_conditions.equals("near")) {
                    priority = 2;
                } else if (signal_conditions.equals("far")) {
                    priority = 3;
                } else {
                    priority = 0;
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


            if (GlobalConfig.isDebug) {


                Log.e("Event Fire Data", ":" + jsonObject);

                Log.e("getUUID", ":" + uuid);
                Log.e("major", ":" + major);
                Log.e("minor", ":" + minor);
                Log.e("signal_conditions", ":" + signal_conditions);
                Log.e("region_condition", ":" + region_condition);
                Log.e("priority", ":" + priority);


            }


            // Get Event Data
            JSONObject action_data = jsonObject.optJSONObject("action_data");
            String action = action_data.optString("action");


            // triggers
            JSONArray triggerJsonArray = null;
            try {
                triggerJsonArray = jsonObject.getJSONArray("triggers");
                JSONObject triggerObj = triggerJsonArray.getJSONObject(0);
                JSONObject triggerType = triggerObj.getJSONObject("type");

            } catch (JSONException e) {

                // TODO
            }


            //Get Event ID
            try {
                event_id = jsonObject.getString("event_id");

            } catch (JSONException e) {

                // TODO
            }

            // URL
            if (action.equals("jbcp_open_url")) {
                event_type = "URL";
                event_details = action_data.optString("url");
                Log.d("DEBUG", action_data.optString("url"));
            }

            // Image
            else if (action.equals("jbcp_open_image")) {
                event_type = "Image";
                event_details = action_data.optString("url");
                Log.d("DEBUG", action_data.optString("url"));
            }

            // Key Value
            else if (action.equals("jbcp_custom_key_value")) {
                event_type = "KEY_VALUE";
                event_details = action_data.optString("key_values");
                Log.d("DEBUG", action_data.optString("key_values"));
            }

            // Text
            else if (action.equals("jbcp_open_text")) {
                event_type = "Text";
                event_details = action_data.optString("text");
                Log.d("DEBUG", action_data.optString("text"));
            }




            DataLogRecorder.setDeviceID(uuid);
            DataLogRecorder.setMajor(major);
            DataLogRecorder.setMinor(minor);
            DataLogRecorder.setRssi(0);
            DataLogRecorder.setProximity(priority);

            // setting event type in log records
            DataLogRecorder.setEventContent(event_type);

            // saving to database becappEvent
            manageCustomLog.becappEvent();
            //saving to database

            // getting csv string
            String beacappCSVString = manageCustomLog.getBeacapEventCSV();

            // setting additional log for beacapRecordModel csv
            jbcpManager.setAdditonalLog(beacappCSVString);



        }
    };

    /**
     * Generate the listener
     */
    public BeaconEventListener beaconEventListener = new BeaconEventListener() {
        @Override
        public boolean targetBeaconDetected(BeaconEvent beaconEvent) {
            // IBeacon that are registered in the detection object in the CMS
//            Log.d("DEBUG", beaconEvent.uuid);


            return false;
        }

        @Override
        public boolean nonTargetBeaconDetected(BeaconEvent beaconEvent) {
            // IBeacon that is not a detection target in CMS
//            Log.d("DEBUG", beaconEvent.uuid);


            return false;
        }
    };


}








