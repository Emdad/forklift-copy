package jp.co.jmas.ForkLift.broadcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.util.Log;

import jp.co.jmas.ForkLift.classes.ManageCustomLog;
import jp.co.jmas.ForkLift.classes.DataLogRecorder;
import jp.co.jmas.ForkLift.classes.GlobalConfig;

/**
 * Created by banglafire on 10/17/16.
 */

public class BatteryStatusReceiver extends BroadcastReceiver {

    Context context = null;


    @Override
    public void onReceive(Context context, Intent intent) {

        String batteryRemaining;

        this.context = context;


        /**
         * Get Battery Charging status
         */

        int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;

        if (isCharging) {
            DataLogRecorder.setBatteryStatus(1);
        } else {
            DataLogRecorder.setBatteryStatus(0);
        }



        int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        float batteryLevel = level / (float) scale;

        batteryRemaining = String.valueOf(batteryLevel) + "%";



        //Set Battery level to log ForkLiftLogRecord
        DataLogRecorder.setBatteryLevel(batteryRemaining);


        if(GlobalConfig.isDebug)
        {

            Log.e(GlobalConfig.TAG,"isCharging  Status bool=" + isCharging);
            Log.e(GlobalConfig.TAG,"BatteryStatusReceiver ="+batteryRemaining);
        }



        //Check if device is authorized
        if(DataLogRecorder.isSdkAuthorized()) {

            ManageCustomLog manageCustomLog = new ManageCustomLog();

            //BatteryRecordModel data save to local db and make csvString for send to customlog
            String csvStringChargingEvent = manageCustomLog.chargingEvent();

            //This csv string getting from ForkliftService Class
            DataLogRecorder.csvStringArray.push(csvStringChargingEvent);

        }


    }


}
