package jp.co.jmas.ForkLift.classes;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.support.v4.content.LocalBroadcastManager;

import jp.co.jmas.ForkLift.broadcasts.BatteryStatusReceiver;

/**
 * Created by banglafire on 10/25/16.
 */

public class BetteryManagersHelper {



    /**
     * getBatteryLevel
     * @param context
     * @return
     */
    public static String getBatteryLevel(Context context) {

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        float batteryLevel = level / (float) scale;

        String batteryRemaining = String.valueOf(batteryLevel) + "%";

        DataLogRecorder.setBatteryLevel(batteryRemaining);

        return batteryRemaining;

    }
}
