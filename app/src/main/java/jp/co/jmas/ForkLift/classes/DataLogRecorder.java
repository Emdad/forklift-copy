package jp.co.jmas.ForkLift.classes;

import java.util.ArrayDeque;

/**
 * Created by banglafire on 10/4/16.
 */
public class DataLogRecorder {

    private static String forkno = "";
    private static String userno = "";
    private static String departname="";
    private static String deviceID="";
    private static int workingflg;
    private static int Tsuminiflg;
    private static String workingSequence="";
    private static String uuid="";
    private static String timeStamp="";
    private static int major;
    private static int minor;
    private static int proximity;
    private static int rssi;
    private static boolean monitorBatteryCharging;
    private static int batteryStatus = 3; // 3 = unknown
    private static String batteryLevel="";
    private static String eventContent="";
    private static boolean sdkAuthorized;

    public static ArrayDeque<String> csvStringArray = new ArrayDeque<>();




    public static String getForkno() {
        return forkno;
    }



    public static void setForkno(String forkno) {
        DataLogRecorder.forkno = forkno;
    }

    public static String getUserno() {
        return userno;
    }

    public static void setUserno(String userno) {
        DataLogRecorder.userno = userno;
    }

    public static String getDepartname() {
        return departname;
    }

    public static void setDepartname(String departname) {
        DataLogRecorder.departname = departname;
    }

    public static String getDeviceID() {
        return deviceID;
    }

    public static void setDeviceID(String deviceID) {
        DataLogRecorder.deviceID = deviceID;
    }


    public static int getWorkingflg() {
        return workingflg;
    }

    public static void setWorkingflg(int workingflg) {
        DataLogRecorder.workingflg = workingflg;
    }

    public static int getTsuminiflg() {
        return Tsuminiflg;
    }

    public static void setTsuminiflg(int tsuminiflg) {
        Tsuminiflg = tsuminiflg;
    }

    public static String getWorkingSequence() {
        return workingSequence;
    }

    public static void setWorkingSequence(String workingSequence) {
        DataLogRecorder.workingSequence = workingSequence;


    }

    public static boolean isMonitorBatteryCharging() {
        return monitorBatteryCharging;
    }

    public static void setMonitorBatteryCharging(boolean monitorBatteryCharging) {
        DataLogRecorder.monitorBatteryCharging = monitorBatteryCharging;
    }

    public static int getBatteryStatus() {

        return batteryStatus;
    }


    /**
     * Set battery status from : BatteryStatusReceiver
     * @param batteryStatus
     */
    public static void setBatteryStatus(int batteryStatus) {

        DataLogRecorder.batteryStatus = batteryStatus;
    }


    /**
     * getting timeStamp from
     * Timestamp data get data without decimal format
     * @return
     */
    public static String getTimeStamp() {

        long current_get_time_unixtime = System.currentTimeMillis() / 1000L;

        return String.valueOf(current_get_time_unixtime);
    }

    public static String getUuid() {
        return uuid;
    }

    public static void setUuid(String uuid) {
        DataLogRecorder.uuid = uuid;
    }

    public static int getMajor() {
        return major;
    }

    public static void setMajor(int major) {
        DataLogRecorder.major = major;
    }

    public static int getMinor() {
        return minor;
    }

    public static void setMinor(int minor) {
        DataLogRecorder.minor = minor;
    }

    public static int getProximity() {
        return proximity;
    }

    public static void setProximity(int proximity) {
        DataLogRecorder.proximity = proximity;
    }

    public static int getRssi() {
        return rssi;
    }

    public static void setRssi(int rssi) {
        DataLogRecorder.rssi = rssi;
    }




    /**
     * Set battery level from : BatteryStatusReceiver
     */

    public static String getBatteryLevel() {
        return batteryLevel;
    }

    public static void setBatteryLevel(String batteryLevel) {
        DataLogRecorder.batteryLevel = batteryLevel;
    }

    /**
     *
     * @return
     */
    public static String getEventContent() {
        return eventContent;
    }

    /**
     *
     *
     * @param eventContent
     */
    public static void setEventContent(String eventContent) {
        DataLogRecorder.eventContent = eventContent;
    }

    public static boolean isSdkAuthorized() {
        return sdkAuthorized;
    }

    public static void setSdkAuthorized(boolean sdkAuthorized) {
        DataLogRecorder.sdkAuthorized = sdkAuthorized;
    }
}
