package jp.co.jmas.ForkLift.classes;

import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by banglafire on 10/5/16.
 */
public class GlobalConfig {


    public final static String SETTINGS_PASSWORD = "3431";
    public final static boolean isDebug = true;
    public   final static String TAG = "ForkLiftLog";

    private  static  String workingSequence;



    public static String getWorkingSequenceDate()
    {
        //Get Current datetime
        DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        workingSequence = df.format(Calendar.getInstance().getTime());
        return workingSequence;

    }




}
