package jp.co.jmas.ForkLift.classes;


import android.util.Log;

import jp.co.jmas.ForkLift.model.BatteryRecordModel;
import jp.co.jmas.ForkLift.model.BeacappRecordModel;
import jp.co.jmas.ForkLift.model.BeaconRecordModel;
import jp.co.jmas.ForkLift.model.KadoRecordModel;
import jp.co.jmas.ForkLift.model.TsuminiRecordModel;
import jp.co.jmas.ForkLift.model.ApplicationRecordModel;

/**
 * Created by Dell on 10/19/2016.
 */

public class ManageCustomLog {


    // initializing table objects
    BeaconRecordModel beaconRecordModel = new BeaconRecordModel();
    BeacappRecordModel beacappRecordModel = new BeacappRecordModel();
    KadoRecordModel kadoRecordModel = new KadoRecordModel();
    TsuminiRecordModel tsuminiRecordModel = new TsuminiRecordModel();
    BatteryRecordModel batteryRecordModel = new BatteryRecordModel();
    ApplicationRecordModel applicationRecordModel = new ApplicationRecordModel();


    /**
     * saving data to BeaconRecordModel
     */
    public void beaconLogging() {

        beaconRecordModel.setWorkingSequence(DataLogRecorder.getWorkingSequence());
        beaconRecordModel.setForkno(DataLogRecorder.getForkno());
        beaconRecordModel.setUserno(DataLogRecorder.getUserno());
        beaconRecordModel.setTimestamp(DataLogRecorder.getTimeStamp());
        beaconRecordModel.setWorkingflg(DataLogRecorder.getWorkingflg());
        beaconRecordModel.setTsuminiflg(DataLogRecorder.getTsuminiflg());
        beaconRecordModel.setUuid(DataLogRecorder.getUuid());
        beaconRecordModel.setMajor(DataLogRecorder.getMajor());
        beaconRecordModel.setMinor(DataLogRecorder.getMinor());
        beaconRecordModel.setProximity(DataLogRecorder.getProximity());
        beaconRecordModel.setRssi(DataLogRecorder.getRssi());
        //Record Save to Database
        long saveid = beaconRecordModel.save();



        /**
         * See log of BeaconRecord
         */

        if (GlobalConfig.isDebug) {

            Log.e(GlobalConfig.TAG,"BeaconRecord: Log Data ");
            Log.d(GlobalConfig.TAG,"BeaconRecord: forkno =" + DataLogRecorder.getForkno());
            Log.d(GlobalConfig.TAG,"BeaconRecord: userno =" + DataLogRecorder.getUserno());
            Log.d(GlobalConfig.TAG,"BeaconRecord: timestamp =" + DataLogRecorder.getTimeStamp());
            Log.d(GlobalConfig.TAG,"BeaconRecord: workingflg =" + DataLogRecorder.getWorkingflg());
            Log.d(GlobalConfig.TAG,"BeaconRecord: tsuminiflg =" + DataLogRecorder.getTsuminiflg());
            Log.d(GlobalConfig.TAG,"BeaconRecord: Beacon UUID =" + DataLogRecorder.getDeviceID());
            Log.d(GlobalConfig.TAG,"BeaconRecord: Beacon Major =" + DataLogRecorder.getMajor());
            Log.d(GlobalConfig.TAG,"BeaconRecord: Beacon Minor =" + DataLogRecorder.getMinor());
            Log.d(GlobalConfig.TAG,"BeaconRecord: Beacon RSSI =" + DataLogRecorder.getRssi());
            Log.d(GlobalConfig.TAG,"BeaconRecord: Beacon Proximity =" + DataLogRecorder.getProximity());
            Log.d(GlobalConfig.TAG,"BeaconRecord: batterystatus =" + DataLogRecorder.getBatteryStatus());
            Log.d(GlobalConfig.TAG,"BeaconRecord: betterylevel =" + DataLogRecorder.getBatteryLevel());


            Log.e(GlobalConfig.TAG,"BeaconRecord: Table(beaconrecord) " + "Last InsertID : " + saveid);
        }

        /**
         * End see log of BeaconRecord
         */



    }



    /**
     * calling method for saving data in becappRecord  table in database and in sdk csv file
     */
    public void becappEvent() {

        beacappRecordModel.setDeviceID(DataLogRecorder.getDeviceID());
        beacappRecordModel.setWorkingSequence((DataLogRecorder.getWorkingSequence()));
        beacappRecordModel.setForkno(DataLogRecorder.getForkno());
        beacappRecordModel.setUserno(DataLogRecorder.getUserno());
        beacappRecordModel.setTimestamp(DataLogRecorder.getTimeStamp());
        beacappRecordModel.setWorkingflg((long) DataLogRecorder.getWorkingflg());
        beacappRecordModel.setTsuminiflg((long) DataLogRecorder.getTsuminiflg());
        beacappRecordModel.setEventvalue(DataLogRecorder.getEventContent());

        long saveid = beacappRecordModel.save();


        /**
         * See log of beacappRecordModel
         */

        if (GlobalConfig.isDebug) {


            Log.e(GlobalConfig.TAG,"BeacappRecord: Log Data ");

            Log.d(GlobalConfig.TAG,"BeacappRecord: DeviceID =" + DataLogRecorder.getDeviceID());
            Log.d(GlobalConfig.TAG,"BeacappRecord: WorkingSequence =" + DataLogRecorder.getWorkingSequence());
            Log.d(GlobalConfig.TAG,"BeacappRecord: forkno =" + DataLogRecorder.getForkno());
            Log.d(GlobalConfig.TAG,"BeacappRecord: userno =" + DataLogRecorder.getUserno());
            Log.d(GlobalConfig.TAG,"BeacappRecord: timestamp =" + DataLogRecorder.getTimeStamp());
            Log.d(GlobalConfig.TAG,"BeacappRecord: workingflg =" + DataLogRecorder.getWorkingflg());
            Log.d(GlobalConfig.TAG,"BeacappRecord: tsuminiflg  =" + DataLogRecorder.getTsuminiflg());
            Log.d(GlobalConfig.TAG,"BeacappRecord: eventContent =" + DataLogRecorder.getEventContent());


            Log.e(GlobalConfig.TAG,"BeacappRecord: Table(beacapprecord) " + "Last InsertID : " + saveid);

        }

        /**
         * End see log of beacappRecord
         */


    }

    /**
     * calling method for saving data in kadoRecord table in database and in sdk csv file
     */
    public void workStartEnd() {
        kadoRecordModel.setWorkingSequence(DataLogRecorder.getWorkingSequence());
        kadoRecordModel.setForkNo(DataLogRecorder.getForkno());
        kadoRecordModel.setUserNo(DataLogRecorder.getUserno());
        kadoRecordModel.setTimeStamp(DataLogRecorder.getTimeStamp());
        kadoRecordModel.setStatus((long) DataLogRecorder.getWorkingflg());
        //Record Save to Database
        long saveid = kadoRecordModel.save();


        /**
         * See log of KadoRecord
         */

        if (GlobalConfig.isDebug) {


            Log.e(GlobalConfig.TAG,"KadoRecord: Log Data (WorkStartEnd) ");

            Log.d(GlobalConfig.TAG,"KadoRecord: DeviceID =" + DataLogRecorder.getDeviceID());
            Log.d(GlobalConfig.TAG,"KadoRecord: WorkingSequence =" + DataLogRecorder.getWorkingSequence());
            Log.d(GlobalConfig.TAG,"KadoRecord: forkno =" + DataLogRecorder.getForkno());
            Log.d(GlobalConfig.TAG,"KadoRecord: userno =" + DataLogRecorder.getUserno());
            Log.d(GlobalConfig.TAG,"KadoRecord: timestamp =" + DataLogRecorder.getTimeStamp());
            Log.d(GlobalConfig.TAG,"KadoRecord: WorkingFlg =" + DataLogRecorder.getWorkingflg());

            Log.e(GlobalConfig.TAG,"KadoRecord: Table(kadorecord) " + "Last InsertID : " + saveid);

        }

        /**
         * End see log of KadoRecord
         */


    }

    /**
     * method for getting kadoRecord csv
     * @return
     */
    public String getKadoRecordCSV() {

        //kADO Record send to SDK Additional Log
        String kadoRecordCSV = kadoRecordModel.getCSV();

        return kadoRecordCSV;

    }

    /**
     * method for beacapRecordModel csv
     */

    public String getBeacapEventCSV(){
        String beacapEventCSV = beacappRecordModel.getCSV();

        return beacapEventCSV;
    }


    /**
     * calling method for saving data in tsuminiRecord table in database and in sdk csv file
     */
    public void cargoSituation() {

        tsuminiRecordModel.setWorkingSequence(DataLogRecorder.getWorkingSequence());
        tsuminiRecordModel.setForkno(DataLogRecorder.getForkno());
        tsuminiRecordModel.setUserno(DataLogRecorder.getUserno());
        tsuminiRecordModel.setTimestamp(DataLogRecorder.getTimeStamp());
        tsuminiRecordModel.setTsuminiflg((long) DataLogRecorder.getTsuminiflg());
        long saveid = tsuminiRecordModel.save();

        /**
         * See log of TsuminiRecord
         */

        if (GlobalConfig.isDebug) {



            Log.e(GlobalConfig.TAG,"TsuminiRecord: Log Data (CargoSituation) ");

            Log.d(GlobalConfig.TAG,"TsuminiRecord: DeviceID =" + DataLogRecorder.getDeviceID());
            Log.d(GlobalConfig.TAG,"TsuminiRecord: WorkingSequence =" + DataLogRecorder.getWorkingSequence());
            Log.d(GlobalConfig.TAG,"TsuminiRecord: forkno =" + DataLogRecorder.getForkno());
            Log.d(GlobalConfig.TAG,"TsuminiRecord: userno =" + DataLogRecorder.getUserno());
            Log.d(GlobalConfig.TAG,"TsuminiRecord: timestamp =" + DataLogRecorder.getTimeStamp());
            Log.d(GlobalConfig.TAG,"TsuminiRecord: tsuminiflg =" + DataLogRecorder.getTsuminiflg());

            Log.e(GlobalConfig.TAG,"TsuminiRecord: TsuminiRecord :Table: " + "Last InsertID : " + saveid);

        }

        /**
         * End see log of TsuminiRecord
         */

    }

    public String getTsuminiRecordCSV() {

        //kADO Record send to SDK Additional Log
        String tsuminiRecordCSV = tsuminiRecordModel.getCSV();

        return tsuminiRecordCSV;

    }

    /**
     * calling method for saving data in battery record table in database and in sdk csv file
     */
    public String chargingEvent() {

        batteryRecordModel.setWorkingSequence(DataLogRecorder.getWorkingSequence());
        batteryRecordModel.setForkno(DataLogRecorder.getForkno());
        batteryRecordModel.setUserno(DataLogRecorder.getUserno());
        batteryRecordModel.setTimestamp(DataLogRecorder.getTimeStamp());
        batteryRecordModel.setWorkConditions((long) DataLogRecorder.getWorkingflg());
        batteryRecordModel.setStatus((long) DataLogRecorder.getBatteryStatus());
        long saveid = batteryRecordModel.save();

        /**
         * See log of BatteryRecord
         */

        if (GlobalConfig.isDebug) {


            Log.e(GlobalConfig.TAG,"BatteryRecord: Log Data (ChargingEvent) ");

            Log.d(GlobalConfig.TAG,"BatteryRecord: DeviceID =" + DataLogRecorder.getDeviceID());
            Log.d(GlobalConfig.TAG,"BatteryRecord: WorkingSequence =" + DataLogRecorder.getWorkingSequence());
            Log.d(GlobalConfig.TAG,"BatteryRecord: forkno =" + DataLogRecorder.getForkno());
            Log.d(GlobalConfig.TAG,"BatteryRecord: userno =" + DataLogRecorder.getUserno());
            Log.d(GlobalConfig.TAG,"BatteryRecord: timestamp =" + DataLogRecorder.getTimeStamp());
            Log.d(GlobalConfig.TAG,"BatteryRecord: batteryStatus =" + DataLogRecorder.getBatteryStatus());

            Log.e(GlobalConfig.TAG,"BatteryRecord: Table(BatteryRecord) " + "Last InsertID : " + saveid);

        }

        /**
         * End see log of BatteryRecord
         */


        return batteryRecordModel.getCSV();


    }


    public String getChargingEventCSV() {

        //batteryRecordCSV Record send to SDK Additional Log
        String batteryRecordCSV = batteryRecordModel.getCSV();

        return batteryRecordCSV;

    }


    /**
     * calling method for saving data in applicationEvent table in database and in sdk csv file
     */
    public void applicationEvent() {

        applicationRecordModel.setDeviceID(DataLogRecorder.getDeviceID());
        applicationRecordModel.setTimestamp(DataLogRecorder.getTimeStamp());

        long saveid = applicationRecordModel.save();

        /**
         * See log of applicationEvent
         */

        if (GlobalConfig.isDebug) {



            Log.e(GlobalConfig.TAG,"ApplicationEvent: Log Data (applicationEvent) ");

            Log.d(GlobalConfig.TAG,"ApplicationEvent: DeviceID =" + DataLogRecorder.getDeviceID());
            Log.d(GlobalConfig.TAG,"ApplicationEvent: timestamp =" + DataLogRecorder.getTimeStamp());


            Log.e(GlobalConfig.TAG,"ApplicationEvent: Table(applicationEvent) " + "Last InsertID : " + saveid);


        }

        /**
         * End see log of applicationEvent
         */


    }

    /**
     * delete method for all rows in tables for sqlite;
     */

    public void deleteModelData(){

        BeacappRecordModel.deleteAll(BeacappRecordModel.class);
        BeaconRecordModel.deleteAll(BeaconRecordModel.class);
        ApplicationRecordModel.deleteAll(ApplicationRecordModel.class);
        TsuminiRecordModel.deleteAll(TsuminiRecordModel.class);
        ApplicationRecordModel.deleteAll(ApplicationRecordModel.class);
        KadoRecordModel.deleteAll(KadoRecordModel.class);

        if(GlobalConfig.isDebug)
        {
            Log.e( GlobalConfig.TAG," Deleted All Database Table Record from local");

        }

    }


}
