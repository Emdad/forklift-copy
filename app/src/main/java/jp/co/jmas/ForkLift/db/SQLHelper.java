package jp.co.jmas.ForkLift.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Dell on 10/10/2016.
 */
public class SQLHelper extends SQLiteOpenHelper {

    // declaring and initializing variables

    // db version
    private static final int DATABASE_VERSION = 1;
    // db name
    private static final String DATABASE_NAME = "ForkLift";

    /**
     * constructor for Database
     *
     * @param context
     */
    public SQLHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * onCreate() method method
     *
     * @param sqLiteDatabase
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // creating  tables
        create_beaconRecord_Table(sqLiteDatabase);
        create_kadoRecord_Table(sqLiteDatabase);
        create_tsuminiRecord_Table(sqLiteDatabase);
        create_beacappRecord_Table(sqLiteDatabase);
        create_batteryRecord_Table(sqLiteDatabase);
        create_applicationRecord_Table(sqLiteDatabase);
        Log.d("creating_table", "all");

    }

    /**
     * creating onUpgrade method
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older beaconRecord table if existed
        db.execSQL("DROP TABLE IF EXISTS beaconRecord");
        db.execSQL("DROP TABLE IF EXISTS kadoRecord");
        db.execSQL("DROP TABLE IF EXISTS tsuminiRecord");
        db.execSQL("DROP TABLE IF EXISTS beacappRecord");
        db.execSQL("DROP TABLE IF EXISTS batteryRecord");
        db.execSQL("DROP TABLE IF EXISTS applicationRecord");

        // create fresh books table
        this.onCreate(db);
    }


    /**
     * method for creeating beaconRecord table
     *
     * @param db
     */
    public void create_beaconRecord_Table(SQLiteDatabase db) {
        // creating beaconRecord table
        String CREATE_BeaconRecord_TABLE = "CREATE TABLE beaconRecord ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "deviceID TEXT, " +
                "workingSequence TEXT, " +
                "forkno TEXT, " +
                "userno TEXT, " +
                "timestamp TEXT, " +
                "workingflg INTEGER, " +
                "tsuminiflg INTEGER, " +
                "uuid TEXT, " +
                "major INTEGER, " +
                "minor INTEGER, " +
                "rssi INTEGER, " +
                "proximity INTEGER, " +
                "batteryStatus INTEGER, " +
                "batteryLevel TEXT )";

        db.execSQL(CREATE_BeaconRecord_TABLE);
        Log.d("creating_table", "CREATE_BeaconRecord_TABLE");
    }


    /**
     * method for creeating kadoRecord table
     *
     * @param db
     */
    public void create_kadoRecord_Table(SQLiteDatabase db) {
        // creating beaconRecord table
        String CREATE_KadoRecord_TABLE = "CREATE TABLE kadoRecord ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "deviceID TEXT, " +
                "workingSequence TEXT, " +
                "forkno TEXT, " +
                "userno TEXT, " +
                "timestamp TEXT, " +
                "status INTEGER )";

        db.execSQL(CREATE_KadoRecord_TABLE);
        Log.d("creating_table", "create_kadoRecord_Table");
    }


    /**
     * method for creeating tsuminiRecord table
     *
     * @param db
     */
    public void create_tsuminiRecord_Table(SQLiteDatabase db) {
        // creating beaconRecord table
        String CREATE_TsuminiRecord_TABLE = "CREATE TABLE tsuminiRecord ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "deviceID TEXT, " +
                "workingSequence TEXT, " +
                "forkno TEXT, " +
                "userno TEXT, " +
                "timestamp TEXT, " +
                "tsuminiflg INTEGER )";

        db.execSQL(CREATE_TsuminiRecord_TABLE);
        Log.d("creating_table", "CREATE_TsuminiRecord_TABLE");
    }


    /**
     * method for creeating beacappRecord table
     *
     * @param db
     */
    public void create_beacappRecord_Table(SQLiteDatabase db) {
        // creating beaconRecord table
        String CREATE_BeacappRecord_TABLE = "CREATE TABLE beacappRecord ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "deviceID TEXT, " +
                "workingSequence TEXT, " +
                "forkno TEXT, " +
                "userno TEXT, " +
                "timestamp TEXT, " +
                "workingflg INTEGER, " +
                "tsuminiflg INTEGER, " +
                "eventvalue TEXT )";

        db.execSQL(CREATE_BeacappRecord_TABLE);
        Log.d("creating_table", "CREATE_BeacappRecord_TABLE");
    }


    /**
     * method for creeating batteryRecord table
     *
     * @param db
     */
    public void create_batteryRecord_Table(SQLiteDatabase db) {
        // creating beaconRecord table
        String CREATE_BatteryRecord_TABLE = "CREATE TABLE batteryRecord ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "deviceID TEXT, " +
                "workingSequence TEXT, " +
                "forkno TEXT, " +
                "userno TEXT, " +
                "timestamp TEXT, " +
                "status INTEGER )";

        db.execSQL(CREATE_BatteryRecord_TABLE);
        Log.e("creating_table", "CREATE_BatteryRecord_TABLE");
    }


    /**
     * method for creeating applicationRecord table
     *
     * @param db
     */
    public void create_applicationRecord_Table(SQLiteDatabase db) {
        // creating beaconRecord table
        String CREATE_ApplicationRecord_TABLE = "CREATE TABLE applicationRecord ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "deviceID TEXT, " +
                "timestamp TEXT)";

        db.execSQL(CREATE_ApplicationRecord_TABLE);
        Log.d("creating_table", "CREATE_ApplicationRecord_TABLE");
    }


//    /**
//     * delete all rows from all the tables
//     */
//
//    public void deleteData(SQLiteDatabase db){
//
//        db.delete("beaconRecord",null,null);
//        db.delete("kadoRecord",null,null);
//        db.delete("tsuminiRecord",null,null);
//        db.delete("beacappRecord",null,null);
//        db.delete("batteryRecord",null,null);
//        db.delete("applicationRecord",null,null);
//
//    }
}
