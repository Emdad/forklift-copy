package jp.co.jmas.ForkLift.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import jp.co.jmas.ForkLift.R;

import jp.co.jmas.ForkLift.activity.MainActivity;
import jp.co.jmas.ForkLift.activity.SettingsActivity;

import jp.co.jmas.ForkLift.classes.GlobalConfig;

import static android.os.Build.VERSION_CODES.M;

public class InitialScreenFragment extends Fragment {


    private BluetoothAdapter BA;

    Button btnSettings;
    EditText txtForkno;
    String requestToken, secretKey;

    SharedPreferences getSharedPref;
    AlertDialog loadingDialog;
    MenuItem removeSettings, cancelMenu, confirm_menu;

    private boolean granted = false;


    //Make interface....
    private onSdkInitializeListener onSdkInitializeListener;

    public interface onSdkInitializeListener {
        public void sdkInitializeCall(String forkno);
    }

    public void onSdkInitilize(String forkno) {
        onSdkInitializeListener.sdkInitializeCall(forkno);
    }
    //End make interface...


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_initialscreen, container, false);

        MainActivity activity = (MainActivity) getActivity();


        // Bluetooth adapter object
        BA = BluetoothAdapter.getDefaultAdapter();
        //Make Instance for sharepreference

        getSharedPref = getActivity().getSharedPreferences("forklift", getActivity().MODE_PRIVATE);
        this.requestToken = getSharedPref.getString("QrCodeForRequestToken", "");
        this.secretKey = getSharedPref.getString("QrCodeForSecretKey", "");


        //Attached listener sdk initialize
        if (getActivity() instanceof onSdkInitializeListener) {
            onSdkInitializeListener = (onSdkInitializeListener) getActivity();
        } else {
            throw new ClassCastException(getActivity().toString() + " must implement InitialScreenFragment.onSdkInitializeListener");
        }


        txtForkno = (EditText) view.findViewById(R.id.txtForkno);

        btnSettings = (Button) view.findViewById(R.id.btnSettings);
        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String forkno = txtForkno.getText().toString();

                if (forkno.isEmpty()) {


                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    alertDialogBuilder.setTitle(R.string.alert_title_confirm);

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(R.string.fork_no_alert_message)
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {}
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    // show it
                    alertDialog.show();


                } else {

                    //Permission check for Android 6
                    if (Build.VERSION.SDK_INT >= M) {
                        // Call some material design APIs here
                        if (getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED
                                || getActivity().checkSelfPermission(Manifest.permission.BLUETOOTH)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.BLUETOOTH}, 0);
                        } else {
                            if (checkBluetoothWithGps()) {


                                gotoNextStep();


                            }
                        }
                    }

                    // Check Bluetooth and GPS IS Enable
                    else if (checkBluetoothWithGps()) {


                        gotoNextStep();


                    }

                }


            }

        });

        return view;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {


        for (int i = 0; i < permissions.length; i++) {
            String perm = permissions[i];

            // This is for checking Camera Permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                    granted = true;
                } else if (shouldShowRequestPermissionRationale(perm)) {
                    // If user deny permission but not checked never show again.
                    // We need to convince with reason to allow this permission.

                    // take user to settings if press ok button

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    alertDialogBuilder.setMessage(R.string.alert_message_settings_phone)
                            .setTitle(R.string.alert_title_confirm)
                            .setCancelable(false)
                            .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    granted = false;
                                }
                            })
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            granted = false;
                                            startInstalledAppDetailsActivity(getActivity());

                                        }
                                    }

                            );

                    // show it
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();


                    // AlertDialog();
                } else {
                    // If user denies permission with checking never show again.


                    granted = false;
                }
            }


        }


        if (granted) {
            gotoNextStep();
        } else {
            // We notify the user that s/he denied camera permission that is why it is not working.
            Toast.makeText(getActivity(), R.string.camera_permission, Toast.LENGTH_LONG).show();
        }


    }


    private void gotoNextStep() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.sdkInitializeAlertTitle);

        // set dialog message
        alertDialogBuilder
                .setMessage(R.string.sdkInitializeAlertMessage)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        if ((requestToken == null || requestToken.isEmpty()) || (secretKey == null || secretKey.isEmpty())) {
                            // Showing SetAlertDialog

                            setAlertDialog("エラー", "BeacappSDKの初期化に失敗しました。アクティベーションキー、シークレットキーの入力を確認してください。");


                        } else {
                            String forkno = txtForkno.getText().toString();
                            onSdkInitilize(forkno);
                        }


                        // TODO
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();

    }

    /**
     * method for going to settings for forklift in android
     *
     * @param context
     */
    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    /**
     * onCreate method initializing options menu
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }


    /**
     * Utility for checking GPS is on, if off then shows a dialog
     *
     * @return
     */
    private boolean isGPSProviderEnabled() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            //Context context = this;

            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
            dialog.setMessage("Your location provider is disabled, please enable from settings to run this meter.");
            dialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    settingsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getActivity().startActivity(settingsIntent);
                }
            });

            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO
                }
            });

            dialog.show();

            //GPS Provider disabled
            return false;
        } else {
            return true;
        }
    }


    /**
     * Utility for checking bluetooth is on
     *
     * @return
     */
    private boolean checkBluetoothWithGps() {

        boolean bluetoothEnable = false;
        boolean gpsEnable = false;

        //Check Bluetooth is enable or not.

        try {
            if (!BA.isEnabled()) {
                Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(turnOn, 0);
            } else {
                bluetoothEnable = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        //check GPS is enable or not...
        if (isGPSProviderEnabled()) {
            gpsEnable = true;
        }

        return (bluetoothEnable == true && gpsEnable == true);
    }


    /**
     * Common Alert Dialog
     *
     * @param title
     * @param message
     */
    public void setAlertDialog(String title, String message) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        // TODO
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    public void showLoadingDialog(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);

        // create alert dialog
        loadingDialog = alertDialogBuilder.create();
        // show it
        loadingDialog.show();
    }


    public void onCreate_alertMessage() {
        // set dialog message
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.setting_password_dialog, null);
        dialogBuilder.setView(dialogView);
        final EditText editText = (EditText) dialogView.findViewById(R.id.et_alert_password);
        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (editText.getText().toString().equalsIgnoreCase(GlobalConfig.SETTINGS_PASSWORD)) {
                    // here will be gone to settings fragment
                    Intent intent = new Intent(getActivity(), SettingsActivity.class);
                    startActivity(intent);

                } else {
                    wrongPassword_alertMessage();

                }
            }
        });
        dialogBuilder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        dialogBuilder.setCancelable(false);
        final AlertDialog alertDialog = dialogBuilder.create();

        alertDialog.show();

    }


    /**
     * method for wrong password alertMessage
     */
    public void wrongPassword_alertMessage() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());


        // set dialog message
        alertDialogBuilder
                .setTitle(R.string.error_alert_title)
                .setMessage(R.string.alert_wrong_password_message)
                .setCancelable(false)
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        onCreate_alertMessage();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }


    /**
     * adding the options menu items and onClick Listener
     *
     * @param menu
     * @param inflater
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_main, menu);
        removeSettings = menu.findItem(R.id.action_settings);
        cancelMenu = menu.findItem(R.id.action_cancel);
        confirm_menu = menu.findItem(R.id.confirm_menu);
        cancelMenu.setVisible(false);
        confirm_menu.setVisible(false);
        removeSettings.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                onCreate_alertMessage();
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);

    }
}
