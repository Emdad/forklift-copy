package jp.co.jmas.ForkLift.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import jp.co.jmas.ForkLift.R;
import jp.co.jmas.ForkLift.activity.MainActivity;
import jp.co.jmas.ForkLift.broadcasts.BatteryStatusReceiver;
import jp.co.jmas.ForkLift.classes.DataLogRecorder;
import jp.co.jmas.ForkLift.util.ScreenBrightnessNotifier;

import static android.os.Build.VERSION_CODES.M;

public class SettingsFragment extends Fragment {


    // declare objects

    BroadcastReceiver mReceiver = null;
    EditText et_qr1, et_qr2;
    Fragment settingsFragment;
    String qr_secret_key, qr_request_key;
    boolean monitor_charging_status, backLight_dim_status;
    TextView textViewQrCodeRequestToken;
    String temp_request_token, temp_secret_key;
    TextView textViewQrCodeSecretKey;
    TextView tv_set_user_no;
    Switch btn_brightness_dim, btn_monitor_battery_charging;
    SharedPreferences savedSharedPref;
    SharedPreferences.Editor editor;
    Button btnSettings;
    int monitor_charging_status_int_value;
    ScreenBrightnessNotifier screenBrightnessNotifier;
    AlertDialog.Builder alertDialogBuilder;
    // create alert dialog
    AlertDialog alertDialog;
    boolean permissionGranted;
    MenuItem removeSettings, cancelMenu, confirm_menu;

    private final int QR_FOR_REQUEST_TOKEN = 1;
    private final int QR_FOR_SECRET_TOKEN = 2;

    //declaring interface..
    private onReadQrcodeListener qrcodeListener;

    /**
     * implementing interface
     */
    public interface onReadQrcodeListener {
        public void calledQrcodeFragment(String type);
    }

    /**
     * defining calledQrCodeReader fragment
     *
     * @param type
     */
    public void calledQrCodeReader(String type) {
        qrcodeListener.calledQrcodeFragment(type);
    }

    //End make interface...


    // declaring interface
    private deptnoFragmentListener deptnoFragmentListener;

    /**
     * defining deptnoFragmentListener method
     */
    public interface deptnoFragmentListener {
        public void calleddeptnoFragment();
    }

    /**
     * defining calleddeptno method
     */
    public void calleddeptno() {
        deptnoFragmentListener.calleddeptnoFragment();
    }
    //End make interface...


    /**
     * onCreateView() method declaration for fragment
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {

        // initializing objects

        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        settingsFragment = this;

        mReceiver = new BatteryStatusReceiver();

        alertDialogBuilder = new AlertDialog.Builder(getActivity());

        savedSharedPref = getActivity().getSharedPreferences("forklift", getActivity().MODE_PRIVATE);
        editor = savedSharedPref.edit();


        et_qr1 = (EditText) view.findViewById(R.id.et_qr1);
        et_qr2 = (EditText) view.findViewById(R.id.et_qr2);


        btn_brightness_dim = (Switch) view.findViewById(R.id.btn_chk3);
        btn_monitor_battery_charging = (Switch) view.findViewById(R.id.btn_chk1);

        btnSettings = (Button) view.findViewById(R.id.btnSettings);
        screenBrightnessNotifier = new ScreenBrightnessNotifier(getActivity());

        // getting the shared prefs value for the edit text fields and switch
        monitor_charging_status = savedSharedPref.getBoolean("monitor_battery_charging", false);

        //Log.d("battery_charging", String.valueOf(monitor_charging_status));
        backLight_dim_status = savedSharedPref.getBoolean("backlight_dim", false);

        //Log.d("backLight_dim_status", String.valueOf(backLight_dim_status));
        qr_secret_key = savedSharedPref.getString("temp_QrCodeForSecretKey", temp_secret_key);
        et_qr2.setText(qr_secret_key);

        qr_request_key = savedSharedPref.getString("temp_QrCodeForRequestToken", temp_request_token);
        et_qr1.setText(qr_request_key);


        // setting the switch button states for backlight and monitor charging
        if (backLight_dim_status) {
            btn_brightness_dim.setChecked(backLight_dim_status);
        } else {
            btn_brightness_dim.setChecked(backLight_dim_status);
        }

        if (monitor_charging_status) {
            btn_monitor_battery_charging.setChecked(monitor_charging_status);
        } else {
            btn_monitor_battery_charging.setChecked(monitor_charging_status);
        }

        // setting onCheckedListener for backlight dim enabled switch
        btn_brightness_dim.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    backLight_dim_status = b;
                    screenBrightnessNotifier.dim_brightness(backLight_dim_status);


                } else {
                    backLight_dim_status = b;
                    screenBrightnessNotifier.dim_brightness(backLight_dim_status);
                    alertMessage_back_light();

                }

            }
        });


        // setting onCheckedListener for  monitor battery charging switch
        btn_monitor_battery_charging.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    monitor_charging_status = b;
                    monitor_charging_status_int_value = 1;


                } else {
                    monitor_charging_status = b;
                    monitor_charging_status_int_value = 0;


                }
            }
        });


        //Attached listener
        if (getActivity() instanceof onReadQrcodeListener) {
            qrcodeListener = (onReadQrcodeListener) getActivity();
        } else {
            throw new ClassCastException(getActivity().toString() + " must implement SettingsFragment.onReadQrcodeListener");
        }

        if (getActivity() instanceof deptnoFragmentListener) {
            deptnoFragmentListener = (deptnoFragmentListener) getActivity();
        } else {
            throw new ClassCastException(getActivity().toString() + " must implement SettingsFragment.deptnoFragmentListener");
        }


        // initializing and setting onClick Listener for textView
        textViewQrCodeRequestToken = (TextView) view.findViewById(R.id.textViewQrCodeRequestToken);
        textViewQrCodeRequestToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                cameraPermissionCheck(QR_FOR_REQUEST_TOKEN);


            }

        });

        textViewQrCodeSecretKey = (TextView) view.findViewById(R.id.textViewQrCodeSecretKey);
        textViewQrCodeSecretKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cameraPermissionCheck(QR_FOR_SECRET_TOKEN);


            }

        });


        //check if sdk already authorized then QR Code read options will be disabled

        boolean sdk_authorized_flag = savedSharedPref.getBoolean("sdkAuthorized", false);

        if (sdk_authorized_flag == true) {
            et_qr1.setEnabled(false);
            et_qr2.setEnabled(false);

            textViewQrCodeRequestToken.setEnabled(false);
            textViewQrCodeSecretKey.setEnabled(false);
        } else {

            et_qr1.setEnabled(true);
            et_qr2.setEnabled(true);

            textViewQrCodeRequestToken.setEnabled(true);
            textViewQrCodeSecretKey.setEnabled(true);
        }


        tv_set_user_no = (TextView) view.findViewById(R.id.tv_set_user_no);
        tv_set_user_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calleddeptno();

            }

        });

        // implementing settings button onClickListener
        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertMessage();


            }

        });


        return view;
    }


    /**
     * declaring alert message for building alert dialog
     */

    public void alertMessage() {
        // set dialog message
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage(R.string.confirm_settings_screen)
                .setTitle(R.string.alert_title_confirm)
                .setCancelable(false)
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                //set log record for battery status
                                DataLogRecorder.setBatteryStatus(monitor_charging_status_int_value);

                                // storing the values in shared preference
                                editor.putBoolean("monitor_battery_charging", monitor_charging_status);
                                editor.putBoolean("backlight_dim", backLight_dim_status);
                                editor.putString("QrCodeForRequestToken", et_qr1.getText().toString());

                                temp_request_token = et_qr1.getText().toString();
                                editor.putString("QrCodeForSecretKey", et_qr2.getText().toString());
                                temp_secret_key = et_qr2.getText().toString();

                                editor.putString("temp_QrCodeForRequestToken", et_qr1.getText().toString());
                                editor.putString("temp_QrCodeForSecretKey", et_qr2.getText().toString());
                                editor.commit();

                                // getting the text value in string variable
                                temp_secret_key = et_qr2.getText().toString();
                                temp_request_token = et_qr1.getText().toString();


                                if (monitor_charging_status) {

                                    //Set log record monitor battery charging
                                    DataLogRecorder.setMonitorBatteryCharging(true);

                                } else {


                                    DataLogRecorder.setMonitorBatteryCharging(false);

                                }


                                // creating intent to go to main activity
                                Intent i = new Intent(getActivity(), MainActivity.class);
                                startActivity(i);

                                // finishing the current activity
                                getActivity().finish();


                            }
                        }
                );

        // creating and showing alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    /**
     * alert message for select  dept no text link
     */

    public void alertMessage_back_light() {
        // set dialog message
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage(R.string.battery_alert_message)
                .setTitle(R.string.alert_title_confirm)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        }
                );

        // creating and showing alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    /**
     * alert message for select  dept no text link
     */

    public void alertMessage_cancel() {
        // set dialog message
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage(R.string.settings_cancel_link_message)
                .setTitle(R.string.alert_title_confirm)
                .setCancelable(false)
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                               // creating intent for main activity class
                                Intent i = new Intent(getActivity(), MainActivity.class);
                                startActivity(i);

                            }
                        }
                );

        // creating and showing alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * initializing the menu item
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // enabling the options menu from fragment
        setHasOptionsMenu(true);
    }

    /**
     * adding the options menu items and onClick Listener
     *
     * @param menu
     * @param inflater
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // clearing the menu
        menu.clear();

        // setting menu options
        inflater.inflate(R.menu.menu_main, menu);
        removeSettings = menu.findItem(R.id.action_settings);
        cancelMenu = menu.findItem(R.id.action_cancel);
        confirm_menu = menu.findItem(R.id.confirm_menu);
        confirm_menu.setVisible(false);
        removeSettings.setVisible(false);

        // setting onclick listener for menu item
        cancelMenu.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                alertMessage_cancel();
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);

    }

    /**
     * method for checking camera permission
     *
     * @param requestCode
     */
    public void cameraPermissionCheck(int requestCode) {


        // checking the sdk version if above 23
        if (Build.VERSION.SDK_INT >= M) {
            // Call some material design APIs here
            if (getActivity().checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, requestCode);

            } else {
                if (requestCode == QR_FOR_REQUEST_TOKEN) {
                    calledQrCodeReader("requestToken");
                } else if (requestCode == QR_FOR_SECRET_TOKEN) {
                    calledQrCodeReader("secretKey");
                }
            }
        } else {
            if (requestCode == QR_FOR_REQUEST_TOKEN) {
                calledQrCodeReader("requestToken");
            } else if (requestCode == QR_FOR_SECRET_TOKEN) {
                calledQrCodeReader("secretKey");
            }
        }

    }


    /**
     * method for onRequestPermission for permission status
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        for (int i = 0; i < permissions.length; i++) {
            String perm = permissions[i];
            if (perm.equalsIgnoreCase(Manifest.permission.CAMERA)) {
                // This is for checking Camera Permission
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        // If user grants permission.
                        if (requestCode == QR_FOR_REQUEST_TOKEN) {
                            calledQrCodeReader("requestToken");
                        } else if (requestCode == QR_FOR_SECRET_TOKEN) {
                            calledQrCodeReader("secretKey");
                        }

                    } else if (shouldShowRequestPermissionRationale(perm)) {
                        // If user deny permission but not checked never show again.
                        // We need to convince with reason to allow this permission.


                        // TODO: take user to settings if press ok button

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(R.string.alert_message_settings_phone)
                                .setTitle(R.string.alert_title_confirm)
                                .setCancelable(false)
                                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                startInstalledAppDetailsActivity(getActivity());

                                            }
                                        }

                                );

                        // creating and showing alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();



                        // AlertDialog();
                    }
                    // If user denies permission with checking never show again.
                    else {

                        Toast.makeText(getActivity(), R.string.camera_permission, Toast.LENGTH_LONG).show();
                    }
                }
            }

        }


    }

    /**
     * method for going to settings for forklift in android
     *
     * @param context
     */
    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        // going to the application settings page in the system
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }
}

