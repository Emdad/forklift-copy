package jp.co.jmas.ForkLift.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import jp.co.jmas.ForkLift.model.DeptListModel;
import jp.co.jmas.ForkLift.R;
import jp.co.jmas.ForkLift.classes.DataLogRecorder;
import jp.co.jmas.ForkLift.classes.ManageCustomLog;
import jp.co.jmas.ForkLift.classes.GlobalConfig;

public class StartWorkScreenFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {


    // declaring objects and variables;
    MenuItem removeSettings, cancelMenu, confirm_menu;


    TextView txtViewForkno;
    EditText editTextUserno;
    Button btnStart;

    LinearLayout linearLayoutUserno;
    RelativeLayout relativeLayoutDepartmentno;


    SharedPreferences savedSharedPref;
    SharedPreferences.Editor editor;


    ManageCustomLog manageCustomLog;


    boolean usernoActive = false;
    String lastSelectedDeptno;

    private onUsernoFragmentListener listener;
    private onUsernoOnGoBackListener backlistener;


    /**
     * onUsernoFragmentListener interface implementation
     */
    public interface onUsernoFragmentListener {
        public void startWork(boolean flag);
    }


    /**
     * declaring goToCargoSwtichFragment() method
     *
     * @param flag
     */
    public void goToCargoSwtichFragment(Boolean flag) {
        listener.startWork(flag);
    }


    /**
     * onUsernoOnGoBackListener() interface implementation
     */
    public interface onUsernoOnGoBackListener {
        public void onCalledSettingsForknofragment(boolean flag);
    }


    /**
     * declaring backToSettings() method
     *
     * @param flag
     */
    public void backToSettings(Boolean flag) {
        backlistener.onCalledSettingsForknofragment(flag);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_startworkscreen, container, false);

        savedSharedPref = getActivity().getSharedPreferences("forklift", getActivity().MODE_PRIVATE);
        editor = savedSharedPref.edit();
        manageCustomLog = new ManageCustomLog();


        // DemoObject.updateSDDDK();


        //Attached listener
        if (getActivity() instanceof onUsernoFragmentListener) {
            listener = (onUsernoFragmentListener) getActivity();
        } else {
            throw new ClassCastException(getActivity().toString() + " must implement StartWorkScreenFragment.onUsernoFragmentListener");
        }

        //Attached listener
        if (getActivity() instanceof onUsernoOnGoBackListener) {
            backlistener = (onUsernoOnGoBackListener) getActivity();
        } else {
            throw new ClassCastException(getActivity().toString() + " must implement StartWorkScreenFragment.onUsernoOnGoBackListener");
        }


        savedSharedPref = getActivity().getSharedPreferences("forklift", getActivity().MODE_PRIVATE);
        usernoActive = savedSharedPref.getBoolean("work_number_switch", false);
        lastSelectedDeptno = savedSharedPref.getString("lastSelecteDeptno", "");

        String getForkno = DataLogRecorder.getForkno();

        //Log.e("Fork NO ",":"+getForkno);
        //Log.e("usernoActive",":"+usernoActive);

        txtViewForkno = (TextView) view.findViewById(R.id.txtViewForkno);
        txtViewForkno.append(getForkno);

        editTextUserno = (EditText) view.findViewById(R.id.editTextUserno);


        //USERNO VISIBLE INVISIBLE
        linearLayoutUserno = (LinearLayout) view.findViewById(R.id.linearLayoutUserno);
        relativeLayoutDepartmentno = (RelativeLayout) view.findViewById(R.id.relativeLayoutDepartmentno);

        //Log.e("Default Value",":"+ DataLogRecorder.getUserno());


        //Check settings : work number input omitted enable
        if (usernoActive) {
            //Set inital value.
            editTextUserno.setText(DataLogRecorder.getUserno());

            linearLayoutUserno.setVisibility(View.VISIBLE);
            relativeLayoutDepartmentno.setVisibility(View.GONE);
        } else {

            linearLayoutUserno.setVisibility(View.GONE);
            relativeLayoutDepartmentno.setVisibility(View.VISIBLE);
        }


        // Spinner element
        Spinner spinner = (Spinner) view.findViewById(R.id.spinnerDepartmentList);

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
//        List<String> categories = new ArrayList<String>();

        DeptListModel deptListModel = new DeptListModel();
        String[] deptList = deptListModel.getDeptList();


        // Creating adapter for dept_dropdown_spinner_textview
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, deptList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.dept_dropdown_spinner_textview);

        // attaching data adapter to dept_dropdown_spinner_textview
        spinner.setAdapter(dataAdapter);
        if (!lastSelectedDeptno.equals(null)) {
            int spinnerPosition = dataAdapter.getPosition(lastSelectedDeptno);
            spinner.setSelection(spinnerPosition);
        }


        btnStart = (Button) view.findViewById(R.id.btnStart);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (usernoActive) {
                    String userno = editTextUserno.getText().toString();

                    if (userno.isEmpty()) {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setTitle(R.string.alert_title_confirm);

                        // set dialog message
                        alertDialogBuilder
                                .setMessage(R.string.btn_alert_message)
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        // TODO

                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        // show it
                        alertDialog.show();


                    } else {


                        //Set Forklift Log
                        DataLogRecorder.setForkno(DataLogRecorder.getForkno());
                        DataLogRecorder.setUserno(userno);
                        DataLogRecorder.setDeviceID(DataLogRecorder.getDeviceID());
                        DataLogRecorder.setWorkingflg(1);
                        DataLogRecorder.setTsuminiflg(0);
                        DataLogRecorder.setWorkingSequence(GlobalConfig.getWorkingSequenceDate());


                        /**
                         * Get Console Debug Log
                         */

                        if (GlobalConfig.isDebug) {

                            Log.e(GlobalConfig.TAG, "BeaconLogging: StartWork");
                            Log.e(GlobalConfig.TAG, "BeaconLogging: forkno =" + DataLogRecorder.getForkno());
                            Log.e(GlobalConfig.TAG, "BeaconLogging: userno =" + DataLogRecorder.getUserno());
                            Log.e(GlobalConfig.TAG, "BeaconLogging: deviceID =" + DataLogRecorder.getDeviceID());
                            Log.e(GlobalConfig.TAG, "BeaconLogging: workingflg =" + DataLogRecorder.getWorkingflg());
                            Log.e(GlobalConfig.TAG, "BeaconLogging: Tsuminiflg =" + DataLogRecorder.getTsuminiflg());
                            Log.e(GlobalConfig.TAG, "BeaconLogging: workingSequence =" + DataLogRecorder.getForkno());
                        }

                        /**
                         * End Get Console Debug Log
                         */


                        //jump to cargoswitchfragment
                        goToCargoSwtichFragment(true);

                    }

                } else {


                    //Set Forklift Log When Department list select that time userno will empty
                    DataLogRecorder.setForkno(DataLogRecorder.getForkno());
                    DataLogRecorder.setUserno("");
                    DataLogRecorder.setDeviceID(DataLogRecorder.getDeviceID());
                    DataLogRecorder.setWorkingflg(1);
                    DataLogRecorder.setTsuminiflg(0);
                    DataLogRecorder.setWorkingSequence(GlobalConfig.getWorkingSequenceDate());

                    /**
                     * Get Console Debug Log
                     */
                    if (GlobalConfig.isDebug) {

                        Log.e(GlobalConfig.TAG, "BeaconLogging: StartWork");
                        Log.e(GlobalConfig.TAG, "BeaconLogging: forkno =" + DataLogRecorder.getForkno());
                        Log.e(GlobalConfig.TAG, "BeaconLogging: userno =" + DataLogRecorder.getUserno());
                        Log.e(GlobalConfig.TAG, "BeaconLogging: deviceID =" + DataLogRecorder.getDeviceID());
                        Log.e(GlobalConfig.TAG, "BeaconLogging: workingflg =" + DataLogRecorder.getWorkingflg());
                        Log.e(GlobalConfig.TAG, "BeaconLogging: Tsuminiflg =" + DataLogRecorder.getTsuminiflg());
                        Log.e(GlobalConfig.TAG, "BeaconLogging: workingSequence =" + DataLogRecorder.getForkno());
                    }

                    /**
                     * End Get Console Debug Log
                     */

                    goToCargoSwtichFragment(true);


                }
            }

        });


        return view;
    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a dept_dropdown_spinner_textview item
        String departmentno = parent.getItemAtPosition(position).toString();

        editor.putString("lastSelecteDeptno", departmentno);
        editor.commit();

        //Set Log
        DataLogRecorder.setDepartname(departmentno);


    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }


    /**
     * onCreate method initializing options menu
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    /**
     * adding the options menu items and onClick Listener
     *
     * @param menu
     * @param inflater
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_main, menu);
        removeSettings = menu.findItem(R.id.action_settings);
        cancelMenu = menu.findItem(R.id.action_cancel);
        confirm_menu = menu.findItem(R.id.confirm_menu);
        confirm_menu.setVisible(false);
        cancelMenu.setVisible(false);
        removeSettings.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                alertDialogBuilder.setTitle(R.string.alert_title_confirm);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.change_forkno)
                        .setCancelable(false)
                        .setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                                DataLogRecorder.setForkno("");

                                backToSettings(true);


                            }
                        });
                alertDialogBuilder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                    }
                });


                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();

                return false;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

}
