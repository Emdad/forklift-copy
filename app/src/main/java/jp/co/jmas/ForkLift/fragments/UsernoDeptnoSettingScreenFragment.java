package jp.co.jmas.ForkLift.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import jp.co.jmas.ForkLift.adapter.DeptListViewAdapter;
import jp.co.jmas.ForkLift.model.DeptListModel;
import jp.co.jmas.ForkLift.R;
import jp.co.jmas.ForkLift.classes.DataLogRecorder;

public class UsernoDeptnoSettingScreenFragment extends Fragment implements View.OnClickListener {

    // declaring objects and variables

    ListView listView;
    DeptListViewAdapter adapter;
    boolean switch_status;
    Switch switch1;
    TextView tv_enable_works_number, registration_number;
    Button btn_input_user_no, btn_select_dept_no;
    SharedPreferences savedSharedPref;
    SharedPreferences.Editor editor;
    MenuItem removeSettings, cancelMenu, confirm_menu;


    /**
     * declaring onCreateView() method
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // initializing view and listView and setting the adapter
        View view = inflater.inflate(R.layout.fragment_userno_deptno_setting, container, false);


        savedSharedPref = getActivity().getSharedPreferences("forklift", getActivity().MODE_PRIVATE);

        editor = savedSharedPref.edit();

        switch1 = (Switch) view.findViewById(R.id.switch1);

        tv_enable_works_number = (TextView) view.findViewById(R.id.tv_enable_works_number);

        registration_number = (TextView) view.findViewById(R.id.registration_number);

        listView = (ListView) view.findViewById(R.id.lv_registration_dept_no);

        btn_input_user_no = (Button) view.findViewById(R.id.btn_input_user_no);

        btn_select_dept_no = (Button) view.findViewById(R.id.btn_select_dept_no);


        // getting the value from shared preference
        switch_status = savedSharedPref.getBoolean("work_number_switch", false);

        // declaring and initializing data model
        DeptListModel deptListModel = new DeptListModel();
        String[] deptListVariable = deptListModel.getDeptList();

        // setting switch
        // implementing onClick Listener for two buttons
        btn_input_user_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btn_input_user_no.setEnabled(false);
                btn_select_dept_no.setEnabled(true);

                btn_input_user_no.setTextColor(Color.WHITE);

                btn_select_dept_no.setBackgroundResource(R.drawable.select_dept_button_white);
                btn_input_user_no.setBackgroundResource(R.drawable.select_dept_button_blue);

                btn_select_dept_no.setTextColor(Color.parseColor("#FF6697FF"));
                switch1.setEnabled(true);
                listView.setVisibility(View.GONE);

                registration_number.setTextColor(Color.parseColor("#d9d9d9"));
                tv_enable_works_number.setTextColor(Color.BLACK);


            }
        });

        btn_select_dept_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btn_input_user_no.setEnabled(true);

                btn_select_dept_no.setEnabled(false);

                switch1.setEnabled(false);

                btn_input_user_no.setBackgroundResource(R.drawable.select_dept_button_white);

                btn_select_dept_no.setBackgroundResource(R.drawable.select_dept_button_blue);

                btn_select_dept_no.setTextColor(Color.WHITE);

                btn_input_user_no.setTextColor(Color.parseColor("#FF6697FF"));

                listView.setVisibility(View.VISIBLE);

                registration_number.setTextColor(Color.BLACK);

                tv_enable_works_number.setTextColor(Color.parseColor("#d9d9d9"));

            }
        });


        // initializing adapter object
        adapter = new DeptListViewAdapter(getActivity(), deptListVariable);


        // by default onCreate() behavior for button
        btn_input_user_no.setEnabled(false);
        btn_select_dept_no.setEnabled(true);
        btn_input_user_no.setTextColor(Color.WHITE);

        btn_select_dept_no.setBackgroundResource(R.drawable.btn_select_deptno_bg1);
        btn_input_user_no.setBackgroundResource(R.drawable.btn_select_deptno_bg2);
        btn_select_dept_no.setTextColor(Color.parseColor("#FF6697FF"));

        switch1.setEnabled(true);
        listView.setVisibility(View.GONE);
        registration_number.setTextColor(Color.parseColor("#d9d9d9"));
        tv_enable_works_number.setTextColor(Color.BLACK);


        // adding hard coded data rows
        listView.setAdapter(adapter);

        // getting onCreate value for switch from shared pref
        switch1.setChecked(switch_status);

        // saving switch button state in shared preference
        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    switch_status = b;
                    alertMessage_positive();

                } else {
                    switch_status = b;
                    alertMessage_negative();
                }
            }
        });


        return view;
    }

    /**
     * declaring onClick() method
     *
     * @param v
     */
    @Override
    public void onClick(View v) {


    }


    /**
     * alert message for dept no fragment switch positive status
     */

    public void alertMessage_positive() {
        // set dialog message
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage(R.string.alert_message_dept_switch_positive)
                .setTitle(R.string.alert_title_confirm)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        }
                );

        // show it
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * alert message for dept no fragment switch negative status
     */

    public void alertMessage_negative() {
        // set dialog message
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage(R.string.alert_message_dept_switch_negative)
                .setTitle(R.string.alert_title_confirm)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        }
                );

        // show it
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    /**
     * enabling options menu
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    /**
     * creating the option menu
     *
     * @param menu
     * @param inflater
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_main, menu);
        removeSettings = menu.findItem(R.id.action_settings);
        cancelMenu = menu.findItem(R.id.action_cancel);
        confirm_menu = menu.findItem(R.id.confirm_menu);
        cancelMenu.setVisible(false);
        confirm_menu.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                editor.putBoolean("work_number_switch", switch_status);
                editor.commit();

                if (switch_status) {
                    Log.e("Setuserno,", ":" + switch_status);
                    DataLogRecorder.setUserno("00000000");
                    Log.e("ForkLiftLogRecords,", ":" + DataLogRecorder.getUserno());
                }


                // android os system defined service for going back to previous fragment
                getActivity().getFragmentManager().popBackStackImmediate();
                return false;
            }
        });
        removeSettings.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                getActivity().getFragmentManager().popBackStackImmediate();
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }
}
