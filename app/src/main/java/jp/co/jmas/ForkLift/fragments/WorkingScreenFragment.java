package jp.co.jmas.ForkLift.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import jp.co.jmas.ForkLift.classes.DataLogRecorder;
import jp.co.jmas.ForkLift.classes.GlobalConfig;
import jp.co.jmas.ForkLift.classes.ManageCustomLog;
import jp.co.jmas.ForkLift.R;

public class WorkingScreenFragment extends Fragment implements View.OnClickListener {


    Button btnEnd;
    TextView txtViewForkno, txtViewUserno, txtViewDepartment;
    ToggleButton toggleButtonCargoSwitch;
    SharedPreferences savedSharedPref;
    ManageCustomLog manageCustomLog;

    boolean usernoActive = false;


    //Create interface
    private onCargoSwitchListener cargoSwitchListener;
    private cargoSwitchOffOnListener cargoSwitchOffOnListener;


    public interface onCargoSwitchListener {
        public void endWork(boolean flag);
    }

    public void calledGotoStartWorkScreen(Boolean flag) {
        cargoSwitchListener.endWork(flag);
    }



    public interface cargoSwitchOffOnListener {
        public void cargoswitchOffOn();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_workingscreen, container, false);

        savedSharedPref = getActivity().getSharedPreferences("forklift", getActivity().MODE_PRIVATE);
        manageCustomLog = new ManageCustomLog();


        //Attached listener
        if (getActivity() instanceof onCargoSwitchListener) {
            cargoSwitchListener = (onCargoSwitchListener) getActivity();
        } else {
            throw new ClassCastException(getActivity().toString() + " must implement WorkingScreenFragment.cargoSwitchListener");
        }

        //Attached listener
        if (getActivity() instanceof cargoSwitchOffOnListener) {
            cargoSwitchOffOnListener = (cargoSwitchOffOnListener) getActivity();
        } else {
            throw new ClassCastException(getActivity().toString() + " must implement WorkingScreenFragment.cargoSwitchOffOnListener");
        }


        String getForkno = DataLogRecorder.getForkno();
        String getUserno = DataLogRecorder.getUserno();
        String getDepartmentname = DataLogRecorder.getDepartname();



        txtViewForkno = (TextView) view.findViewById(R.id.txtViewForkno);
        txtViewForkno.append(getForkno);

        txtViewUserno = (TextView) view.findViewById(R.id.txtViewUserno);

        txtViewDepartment = (TextView) view.findViewById(R.id.txtViewDepartment);


        usernoActive = savedSharedPref.getBoolean("work_number_switch", false);
        if (usernoActive) {
            txtViewUserno.append(getUserno);
            txtViewUserno.setVisibility(View.VISIBLE);
            txtViewDepartment.setVisibility(View.INVISIBLE);
        } else {
            txtViewDepartment.append(getDepartmentname);

            txtViewUserno.setVisibility(View.INVISIBLE);
            txtViewDepartment.setVisibility(View.VISIBLE);
        }


        toggleButtonCargoSwitch = (ToggleButton) view.findViewById(R.id.toggleButtonCargoSwitch);
        toggleButtonCargoSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                int tsuminiflg = 0;

                if (isChecked) {
                    Log.d(GlobalConfig.TAG,"CargoSwitch: ON");
                    tsuminiflg = 1;

                } else {
                    Log.d(GlobalConfig.TAG,"CargoSwitch: OFF");
                    tsuminiflg = 0;
                }


                //Set Forklift Log
                DataLogRecorder.setForkno(DataLogRecorder.getForkno());
                DataLogRecorder.setUserno(DataLogRecorder.getUserno());
                DataLogRecorder.setDeviceID(DataLogRecorder.getDeviceID());
                DataLogRecorder.setWorkingflg(1);
                DataLogRecorder.setTsuminiflg(tsuminiflg); //set tsuminflg 0 or 1
                DataLogRecorder.setWorkingSequence(GlobalConfig.getWorkingSequenceDate());


                /**
                 * Get Console Debug Log
                 */

                if (GlobalConfig.isDebug) {

                    Log.e(GlobalConfig.TAG,"BeaconLogging: CargoSwitch(OFF/ON)");
                    Log.e(GlobalConfig.TAG,"BeaconLogging: forkno =" + DataLogRecorder.getForkno());
                    Log.e(GlobalConfig.TAG,"BeaconLogging: userno =" + DataLogRecorder.getUserno());
                    Log.e(GlobalConfig.TAG,"BeaconLogging: deviceID =" + DataLogRecorder.getDeviceID());
                    Log.e(GlobalConfig.TAG,"BeaconLogging: workingflg =" + DataLogRecorder.getWorkingflg());
                    Log.e(GlobalConfig.TAG,"BeaconLogging: Tsuminiflg =" + DataLogRecorder.getTsuminiflg());
                    Log.e(GlobalConfig.TAG,"BeaconLogging: workingSequence =" + DataLogRecorder.getWorkingSequence());
                }

                /**
                 * End Get Console Debug Log
                 */


                cargoSwitchOffOnListener.cargoswitchOffOn();


            }
        });


        btnEnd = (Button) view.findViewById(R.id.btnEnd);
        btnEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                alertDialogBuilder.setTitle(R.string.alert_title_confirm);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.cargo_end_message)
                        .setCancelable(false)
                        .setNegativeButton(R.string.cargo_switch_alert_no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .setPositiveButton(R.string.cargo_alert_switch_yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                // TODO

                                //Set Forklift Log
                                DataLogRecorder.setForkno(DataLogRecorder.getForkno());
                                DataLogRecorder.setUserno("");
                                DataLogRecorder.setDeviceID(DataLogRecorder.getDeviceID());
                                DataLogRecorder.setWorkingflg(0);
                                DataLogRecorder.setTsuminiflg(2);
                                DataLogRecorder.setWorkingSequence(GlobalConfig.getWorkingSequenceDate());


                                /**
                                 * Get Console Debug Log
                                 */

                                if (GlobalConfig.isDebug) {

                                    Log.e(GlobalConfig.TAG,"BeaconRangingLog: END WORK");
                                    Log.e(GlobalConfig.TAG,"BeaconRangingLog: forkno =" + DataLogRecorder.getForkno());
                                    Log.e(GlobalConfig.TAG,"BeaconRangingLog: userno =" + DataLogRecorder.getUserno());
                                    Log.e(GlobalConfig.TAG,"BeaconRangingLog: deviceID =" + DataLogRecorder.getDeviceID());
                                    Log.e(GlobalConfig.TAG,"BeaconRangingLog: workingflg =" + DataLogRecorder.getWorkingflg());
                                    Log.e(GlobalConfig.TAG,"BeaconRangingLog: Tsuminiflg =" + DataLogRecorder.getTsuminiflg());
                                    Log.e(GlobalConfig.TAG,"BeaconRangingLog: workingSequence =" + DataLogRecorder.getWorkingSequence());
                                }

                                /**
                                 * End Get Console Debug Log
                                 */


                                calledGotoStartWorkScreen(true);

                            }
                        });


                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();


            }

        });


        return view;
    }

    /**
     * getting the menu items
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    /**
     * clearing menu item
     * @param menu
     * @param inflater
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onClick(View v) {


    }


}
