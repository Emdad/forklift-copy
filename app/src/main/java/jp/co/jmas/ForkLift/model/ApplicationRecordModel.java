package jp.co.jmas.ForkLift.model;

import com.orm.SugarRecord;

/**
 * Created by Dell on 10/10/2016.
 */
public class ApplicationRecordModel extends SugarRecord
{
    //declaring fields for Model
    Long id;
    String deviceID, timestamp;



    /**
     * getter method for id
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     * setter method for id
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * getter method for deviceID
     * @return
     */
    public String getDeviceID() {
        return deviceID;
    }

    /**
     *  setter method for deviceID
     * @param deviceID
     */
    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    /**
     *  getter method for timestamp
     * @return
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     *saving to database using sugar orm
     */


    /**
     * setter method for timestamp
     * @param timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}


