package jp.co.jmas.ForkLift.model;

import android.util.Base64;

import com.orm.SugarRecord;

/**
 * Created by banglafire on 10/17/16.
 */

public class BaseModel extends SugarRecord {

    public String generateCSV(String type, String[] data)
    {


        String csvOutput="";
        String fullString="";

        String strType =  "type:"+type;
        String csvString =  "csvstring:";



        int totallen = data.length;

        for(int i=0; i<totallen; i++)
        {

            if(i == totallen-1)
            {
                //not add , comma separator in last item
                csvString+=data[i];
            }
            else
            {
                csvString+=data[i]+",";
            }


        }

        fullString = strType + "," + csvString;


        try {
            csvOutput = Base64.encodeToString(fullString.getBytes("UTF-8"), Base64.DEFAULT);
        } catch (Exception e) {

        }


        return csvOutput;
    }
}
