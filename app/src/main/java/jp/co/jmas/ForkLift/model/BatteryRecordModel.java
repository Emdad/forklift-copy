package jp.co.jmas.ForkLift.model;

import android.util.Log;

import java.util.Arrays;

import jp.co.jmas.ForkLift.classes.DataLogRecorder;
import jp.co.jmas.ForkLift.classes.GlobalConfig;

/**
 * Created by Dell on 10/10/2016.
 */
public class BatteryRecordModel extends BaseModel
{
    // declaring fields for the model
    String type = "battery";
    Long id, status,workConditions;
    String deviceID, workingSequence, forkno, userno, timestamp;

    /**
     * getter method for id
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     * setter method for id
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * getter method for Charging status
     * @return
     */
    public Long getStatus() {
        return status;
    }

    /**
     *  setter method for Charging status
     * @param status
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getWorkConditions() {
        return workConditions;
    }

    public void setWorkConditions(Long workConditions) {
        this.workConditions = workConditions;
    }

    /**
     * getter method deviceID
     * @return
     */
    public String getDeviceID() {

        deviceID = DataLogRecorder.getDeviceID();
        return deviceID;
    }

    /**
     * setter method deviceID
     * @param deviceID
     */
    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    /**
     * getter method workingSequence
     * @return
     */
    public String getWorkingSequence() {
        return workingSequence;
    }

    /**
     *  setter method workingSequence
     * @param workingSequence
     */
    public void setWorkingSequence(String workingSequence) {
        this.workingSequence = workingSequence;
    }

    /**
     *  getter method forkno
     * @return
     */
    public String getForkno() {
        return forkno;
    }

    /**
     *  setter method forkno
     * @param forkno
     */
    public void setForkno(String forkno) {
        this.forkno = forkno;
    }

    /**
     *  getter method userno
     * @return
     */
    public String getUserno() {
        return userno;
    }

    /**
     * setter method userno
     * @param userno
     */
    public void setUserno(String userno) {
        this.userno = userno;
    }

    /**
     * getter method timestamp
     * @return
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * setter method timestamp
     * @param timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getCSV() {

        String csvStringBase64 = null;

        /**
         *  Sequence, for NO., user No., time stamp, working conditions、Charging status
         */

        String data[] = {

                DataLogRecorder.getWorkingSequence(),
                DataLogRecorder.getForkno(),
                DataLogRecorder.getUserno(),
                DataLogRecorder.getTimeStamp(),
                String.valueOf(DataLogRecorder.getWorkingflg()),
                String.valueOf(DataLogRecorder.getBatteryStatus())

        };




        //Called Generate CSV METHOD
        csvStringBase64 = generateCSV(this.type, data);



        if(GlobalConfig.isDebug)
        {
            Log.e(GlobalConfig.TAG,"BatteryRecord:  Fields =" + "Sequence, fork NO., user No., time stamp, working conditions、Charging status");
            Log.e(GlobalConfig.TAG,"BatteryRecord: DataArray =" + Arrays.toString(data));
            Log.e(GlobalConfig.TAG,"BatteryRecord: CSVFormat =" + "type:battery,csvstring:Sequence, fork NO., user No., time stamp, working conditions、Charging status");
            Log.e(GlobalConfig.TAG,"BatteryRecord: CSVString =" + csvStringBase64);


        }


        return csvStringBase64;

    }


}
