package jp.co.jmas.ForkLift.model;

import android.util.Log;

import java.util.Arrays;

import jp.co.jmas.ForkLift.classes.DataLogRecorder;
import jp.co.jmas.ForkLift.classes.GlobalConfig;

/**
 * Created by Dell on 10/10/2016.
 */
public class BeacappRecordModel extends BaseModel
{
    // declaring fields for the model
    Long  id,workingflg, tsuminiflg;
    String deviceID, workingSequence, forkno, userno, timestamp, eventvalue;
    String type = "beacappEvent";


    /**
     * getter method for id
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     * setter method for id
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * getter method for workingflg
     * @return
     */
    public Long getWorkingflg() {
        return workingflg;
    }

    /**
     * setter method for workingflg
     * @param workingflg
     */
    public void setWorkingflg(Long workingflg) {
        this.workingflg = workingflg;
    }

    /**
     * getter method for tsuminiflg
     * @return
     */
    public Long getTsuminiflg() {
        return tsuminiflg;
    }

    /**
     * setter method for tsuminiflg
     * @param tsuminiflg
     */
    public void setTsuminiflg(Long tsuminiflg) {
        this.tsuminiflg = tsuminiflg;
    }

    /**
     * getter method for deviceID
     * @return
     */
    public String getDeviceID() {
        return deviceID;
    }

    /**
     *  setter method for deviceID
     * @param deviceID
     */
    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    /**
     *  getter method for workingSequence
     * @return
     */
    public String getWorkingSequence() {
        return workingSequence;
    }

    /**
     * setter method for workingSequence
     * @param workingSequence
     */
    public void setWorkingSequence(String workingSequence) {
        this.workingSequence = workingSequence;
    }

    /**
     * getter method for forkno
     * @return
     */
    public String getForkno() {
        return forkno;
    }

    /**
     * setter method for forkno
     * @param forkno
     */
    public void setForkno(String forkno) {
        this.forkno = forkno;
    }

    /**
     * getter method for userno
     * @return
     */
    public String getUserno() {
        return userno;
    }

    /**
     * setter method for userno
     * @param userno
     */
    public void setUserno(String userno) {
        this.userno = userno;
    }

    /**
     * getter method for timestamp
     * @return
     */
    public String getTimestamp() {

        return timestamp;
    }

    /**
     *  setter method for timestamp
     * @param timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     *  getter method for eventvalue
     * @return
     */
    public String getEventvalue() {
        return eventvalue;
    }

    /**
     * setter method for eventvalue
     * @param eventvalue
     */
    public void setEventvalue(String eventvalue) {
        this.eventvalue = eventvalue;
    }


    public String getCSV() {

        String csvStringBase64 = null;


        /**
         *  Sequence, folk No. user No., time stamp, working conditions,  beacon UUID, cargo status, event type
         */
        String data[] = {

                DataLogRecorder.getWorkingSequence(),
                DataLogRecorder.getForkno(),
                DataLogRecorder.getUserno(),
                DataLogRecorder.getTimeStamp(),
                String.valueOf(DataLogRecorder.getWorkingflg()),
                DataLogRecorder.getUuid(),
                String.valueOf(DataLogRecorder.getTsuminiflg()),
                DataLogRecorder.getEventContent()
        };

        //Called Generate CSV METHOD
        csvStringBase64 = generateCSV(this.type, data);


        if(GlobalConfig.isDebug)
        {
            Log.e(GlobalConfig.TAG,"BeacappRecord:  Fields =" + "Sequence, folk No. user No., time stamp, working conditions,  beacon UUID, cargo status, event type");
            Log.e(GlobalConfig.TAG,"BeacappRecord:  DataArray =" + Arrays.toString(data));
            Log.e(GlobalConfig.TAG,"BeacappRecord:  CSVFormat =" + "type:beacapp,csvstring:Sequence, folk No. user No., time stamp, working conditions,  beacon UUID, cargo status, event type");
            Log.e(GlobalConfig.TAG,"BeacappRecord:  CSVString =" + csvStringBase64);


        }



        return csvStringBase64;

    }



}

