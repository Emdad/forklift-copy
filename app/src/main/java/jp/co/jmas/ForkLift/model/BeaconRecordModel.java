package jp.co.jmas.ForkLift.model;


import android.util.Log;

import java.util.Arrays;

import jp.co.jmas.ForkLift.classes.DataLogRecorder;
import jp.co.jmas.ForkLift.classes.GlobalConfig;

/**
 * Created by Dell on 10/10/2016.
 */
public class BeaconRecordModel extends BaseModel {


    // declaring fields for model class


    String type = "beacon";
    Long id;
    int workingflg, tsuminiflg, major, minor, rssi, proximity, batteryStatus;
    String deviceID, workingSequence, forkno, userno, timestamp, uuid,batteryLevel="";



    /**
     * getter method for ID
     *
     * @return
     */
    public Long getId() {
        return id;
    }


    /**
     * setter method for ID
     *
     * @return
     */
    public void setId(Long id) {
        this.id = id;
    }


    /**
     * getter method for workingFlag
     *
     * @return
     */
    public int getWorkingflg() {
        return workingflg;
    }

    /**
     * setter method for workingFlag
     *
     * @param workingflg
     */
    public void setWorkingflg(int workingflg) {
        this.workingflg = workingflg;
    }

    /**
     * getter method for tsuminiflg
     *
     * @return
     */
    public int getTsuminiflg() {
        return tsuminiflg;
    }

    /**
     * setter method for tsuminiflg
     *
     * @param tsuminiflg
     */
    public void setTsuminiflg(int tsuminiflg) {
        this.tsuminiflg = tsuminiflg;
    }

    /**
     * getter method for major
     *
     * @return
     */
    public int getMajor() {
        return major;
    }

    /**
     * setter method for major
     *
     * @param major
     */
    public void setMajor(int major) {
        this.major = major;
    }

    /**
     * getter method for minor
     *
     * @return
     */
    public int getMinor() {
        return minor;
    }

    /**
     * setter method for minor
     *
     * @param minor
     */
    public void setMinor(int minor) {
        this.minor = minor;
    }

    /**
     * getter method for rssi
     *
     * @return
     */
    public int getRssi() {
        return rssi;
    }

    /**
     * setteer method for rssi
     *
     * @param rssi
     */
    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    /**
     * getter method for proximity
     *
     * @return
     */
    public int getProximity() {
        return proximity;
    }

    /**
     * setter method for proximity
     *
     * @param proximity
     */
    public void setProximity(int proximity) {
        this.proximity = proximity;
    }

    /**
     * getter method for batteryStatus
     *
     * @return
     */
    public int getBatteryStatus() {



        /*
        switch (status)

        {
            case chaging:
            case full:
                batteryStatus = 1;
            break;

            case  unplugged:
                batteryStatus = 0;
            break;

            case  unknown:
                batteryStatus = 3;
            break;

        }*/



        if(DataLogRecorder.getBatteryStatus() == 1 || DataLogRecorder.getBatteryStatus() == 0)
        {
            batteryStatus = DataLogRecorder.getBatteryStatus();
        }
        else
        {

            batteryStatus = 3;
        }


        return batteryStatus;
    }

    /**
     * setter method batteryStatus
     *
     * @param batteryStatus
     */
    public void setBatteryStatus(int batteryStatus) {



        this.batteryStatus = batteryStatus;
    }

    /**
     * getter method for deviceID
     *
     * @return
     */
    public String getDeviceID() {
        return deviceID;
    }

    /**
     * setter method for deviceID
     *
     * @param deviceID
     */
    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    /**
     * getter method for workingSequence
     *
     * @return
     */
    public String getWorkingSequence() {
        return workingSequence;
    }

    /**
     * setter method for workingSequence
     *
     * @param workingSequence
     */
    public void setWorkingSequence(String workingSequence) {
        this.workingSequence = workingSequence;
    }

    /**
     * getter method for forkNo
     *
     * @return
     */
    public String getForkno() {
        return forkno;
    }

    /**
     * setter method for  forkNo
     *
     * @param forkno
     */
    public void setForkno(String forkno) {
        this.forkno = forkno;
    }

    /**
     * getter method for userName
     *
     * @return
     */
    public String getUserno() {
        return userno;
    }

    /**
     * setter method for userName
     *
     * @param userno
     */
    public void setUserno(String userno) {
        this.userno = userno;
    }

    /**
     * getter method for timeStamp
     *
     * @return
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * setter method for timeStamp
     *
     * @param timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * getter method for uuid
     *
     * @return
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * setter method for uuid
     *
     * @param uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }


    public String getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(String batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public String getCSV() {

        String csvStringBase64 = null;



        /**
         *  Sequence, folk No. user No., time stamp, charge status, remaining battery capacity,
         *   working conditions, cargo status, beacon UUID, Major, Minor, proximity, RSSI (there is meaning the order.)
         */
        String data[] = {

                DataLogRecorder.getWorkingSequence(),
                DataLogRecorder.getForkno(),
                DataLogRecorder.getUserno(),
                DataLogRecorder.getTimeStamp(),
                String.valueOf(DataLogRecorder.getBatteryStatus()),
                String.valueOf(DataLogRecorder.getBatteryLevel()),
                String.valueOf(DataLogRecorder.getWorkingflg()),
                String.valueOf(DataLogRecorder.getTsuminiflg()),
                DataLogRecorder.getUuid(),
                String.valueOf(DataLogRecorder.getMajor()),
                String.valueOf(DataLogRecorder.getMinor()),
                String.valueOf(DataLogRecorder.getProximity()),
                String.valueOf(DataLogRecorder.getRssi())

        };





        //Called Generate CSV METHOD
        csvStringBase64 = generateCSV(this.type, data);


        if(GlobalConfig.isDebug)
        {
            Log.e(GlobalConfig.TAG,"BeaconRecord: Fields = " + "Sequence, folk No. user No., time stamp, charge status, remaining battery capacity,working conditions, cargo status, beacon UUID, Major, Minor, proximity, RSSI");
            Log.e(GlobalConfig.TAG,"BeaconRecord:  Data Array =" + Arrays.toString(data));
            Log.e(GlobalConfig.TAG,"BeaconRecord:  CSV Format =" + "type:beacon,csvstring:Sequence, folk No. user No., time stamp, charge status, remaining battery capacity, working conditions, cargo status, beacon UUID, Major, Minor, proximity, RSSI");
            Log.e(GlobalConfig.TAG,"BeaconRecord:  CSV String =" + csvStringBase64);


        }


        return csvStringBase64;


    }



}

