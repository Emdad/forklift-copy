package jp.co.jmas.ForkLift.model;

/**
 * Created by Dell on 10/3/2016.
 */
public class DeptListModel {


    private String itemLists[] ={"生産管理Ｇ","鋳造Ｇ","鋳造Ｇ・大型班","鋳造Ｇ・中型班","機械Ｇ","北陽工業","ワークス","山川産業","ツチヨシ産業","その他"};

    /**
     * constructor initilization
     */
        public DeptListModel()
        {

        }

    /**
     * getDeptList() method declaring for getting the list names array
     * @return
     */
        public String[] getDeptList()
        {
                return itemLists;
        }

        public String getDeptItem(int i)
        {
            return itemLists[i-1];
        }




}
