package jp.co.jmas.ForkLift.model;


import android.util.Log;

import java.util.Arrays;

import jp.co.jmas.ForkLift.classes.DataLogRecorder;
import jp.co.jmas.ForkLift.classes.GlobalConfig;

/**
 * Created by Dell on 10/10/2016.
 */
public class KadoRecordModel extends BaseModel
{

    //Kado Means Work Start END

    // declaring fields for model class
    String type = "kado";
    Long id, status;
    String deviceID, forkNo, workingSequence, userNo,timeStamp;


    /**
     * getter method for id
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     * setter method for id
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * getter method for status
     * @return
     */
    public Long getStatus() {
        return status;
    }

    /**
     * setter method for status
     * @param status
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * getter method for deviceID
     * @return
     */
    public String getDeviceID() {
        return deviceID;
    }

    /**
     * setter method for deviceID
     * @param deviceID
     */
    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    /**
     * getter method for forkNo
     * @return
     */
    public String getForkNo() {
        return forkNo;
    }

    /**
     * setter method for forkNo
     * @param forkNo
     */
    public void setForkNo(String forkNo) {
        this.forkNo = forkNo;
    }

    /**
     * getter method for workingSequence
     * @return
     */
    public String getWorkingSequence() {
        return workingSequence;
    }

    /**
     *  setter method for workingSequence
     * @param workingSequence
     */
    public void setWorkingSequence(String workingSequence) {
        this.workingSequence = workingSequence;
    }

    /**
     * getter method for userNo
     * @return
     */
    public String getUserNo() {
        return userNo;
    }

    /**
     *  setter method for userNo
     * @param userNo
     */
    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    /**
     * getter method for timeStamp
     * @return
     */
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     *  setter method for timeStamp
     * @param timeStamp
     */
    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }



    public String getCSV() {

        String csvStringBase64 = null;

        /**
         *  Sequence, for NO., user No., time stamp, working conditions
         */

        String data[] = {

                DataLogRecorder.getWorkingSequence(),
                DataLogRecorder.getForkno(),
                DataLogRecorder.getUserno(),
                DataLogRecorder.getTimeStamp(),
                String.valueOf(DataLogRecorder.getWorkingflg())

        };


        //Called Generate CSV METHOD
        csvStringBase64 = generateCSV(this.type, data);


        if(GlobalConfig.isDebug)
        {
            Log.e(GlobalConfig.TAG,"KadoRecord: Fields = " + "Sequence, fork NO., user No., time stamp, working conditions");
            Log.e(GlobalConfig.TAG,"KadoRecord: DataArray =" + Arrays.toString(data));
            Log.e(GlobalConfig.TAG,"KadoRecord: CSVFormat =" + "type:kado,csvstring:Sequence, fork NO., user No., time stamp, working conditions");
            Log.e(GlobalConfig.TAG,"KadoRecord: CSVString =" + csvStringBase64);


        }

        return csvStringBase64;

    }

}
