package jp.co.jmas.ForkLift.model;


import android.util.Log;

import java.util.Arrays;

import jp.co.jmas.ForkLift.classes.DataLogRecorder;
import jp.co.jmas.ForkLift.classes.GlobalConfig;

/**
 * Created by Dell on 10/10/2016.
 */
public class TsuminiRecordModel extends BaseModel
{
    //TsuminiRecord means

    // declaring fields for model class
    String type = "tsumini";
    Long  id, tsuminiflg;
    String deviceID, workingSequence, forkno, userno, timestamp;

    /**
     * getter method for id
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     * setter method for id
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * getter method for tsuminiflg
     * @return
     */
    public Long getTsuminiflg() {
        return tsuminiflg;
    }

    /**
     * setter method for tsuminiflg
     * @param tsuminiflg
     */
    public void setTsuminiflg(Long tsuminiflg) {
        this.tsuminiflg = tsuminiflg;
    }

    /**
     *  getter method for deviceID
     * @return
     */
    public String getDeviceID() {
        return deviceID;
    }

    /**
     * setter method for deviceID
     * @param deviceID
     */
    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    /**
     * getter method for workingSequence
     * @return
     */
    public String getWorkingSequence() {
        return workingSequence;
    }

    /**
     * setter method for workingSequence
     * @param workingSequence
     */
    public void setWorkingSequence(String workingSequence) {
        this.workingSequence = workingSequence;
    }

    /**
     * getter method for forkno
     * @return
     */
    public String getForkno() {
        return forkno;
    }

    /**
     * setter method for forkno
     * @param forkno
     */
    public void setForkno(String forkno) {
        this.forkno = forkno;
    }

    /**
     * getter method for userno
     * @return
     */
    public String getUserno() {
        return userno;
    }

    /**
     * setter method for userno
     * @param userno
     */
    public void setUserno(String userno) {
        this.userno = userno;
    }

    /**
     * getter method for timestamp
     * @return
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * setter method for timestamp
     * @param timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }



    public String getCSV() {

        String csvStringBase64 = null;

        /**
         *  Sequence, for NO., user No., time stamp, cargo situation(Tsumini)
         */

        String data[] = {

                DataLogRecorder.getWorkingSequence(),
                DataLogRecorder.getForkno(),
                DataLogRecorder.getUserno(),
                DataLogRecorder.getTimeStamp(),
                String.valueOf(DataLogRecorder.getTsuminiflg())

        };



        //Called Generate CSV METHOD
        csvStringBase64 = generateCSV(this.type, data);


        if(GlobalConfig.isDebug)
        {
            Log.e(GlobalConfig.TAG,"TsuminiRecord: Fields =" + "Sequence, fork NO., user No., time stamp, cargo situation(Tsumini)");
            Log.e(GlobalConfig.TAG,"TsuminiRecord:  DataArray =" + Arrays.toString(data));
            Log.e(GlobalConfig.TAG,"TsuminiRecord:  CSVFormat  =" + "type:tsumini,csvstring:Sequence, fork NO., user No., time stamp, cargo situation(Tsumini)");
            Log.e(GlobalConfig.TAG,"TsuminiRecord:  CSVString  =" + csvStringBase64);


        }



        return csvStringBase64;

    }

}
