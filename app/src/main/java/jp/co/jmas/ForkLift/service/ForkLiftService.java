package jp.co.jmas.ForkLift.service;

/**
 * Created by banglafire on 10/16/16.
 */

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;


import jp.co.jmas.ForkLift.beaconSDK.SDKTaskManager;
import jp.co.jmas.ForkLift.classes.BetteryManagersHelper;
import jp.co.jmas.ForkLift.classes.ManageCustomLog;
import jp.co.jmas.ForkLift.model.BeaconRecordModel;
import jp.co.jmas.ForkLift.classes.DataLogRecorder;
import jp.co.jmas.ForkLift.classes.GlobalConfig;


public class ForkLiftService extends Service {



    private Intent intent;
    Context context = this;


    //we are going to use a handler to be able to run in our TimerTask
    private final Handler handler = new Handler();


    //Model define
    ManageCustomLog manageCustomLog;
    BeaconRecordModel beaconRecordModel;
    SDKTaskManager sdkTaskManagerObj;

    /**
     * defining onCreate for service
     */
    @Override
    public void onCreate() {

        sdkTaskManagerObj = new SDKTaskManager(context);
        sdkTaskManagerObj.sdkInitilizeSettings();
        // initializing objects

        beaconRecordModel = new BeaconRecordModel();
        manageCustomLog = new ManageCustomLog();


    }


    /**
     * defining onStartCommand()
     *
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        startTimer();

        return Service.START_NOT_STICKY;
    }


    /**
     * defining onBind method
     *
     * @param arg0
     * @return
     */
    @Override
    public IBinder onBind(Intent arg0) {

        return null;
    }


    /**
     * defining startTime method for timer task
     */
    public void startTimer() {

        handler.removeCallbacks(timerTask);
        handler.postDelayed(timerTask, 1000); // 1 second
    }


    private Runnable timerTask = new Runnable() {
        public void run() {
            sendUpdatesToServer();
            handler.postDelayed(this, 1000); // 1 seconds
        }
    };


    private void sendUpdatesToServer(){

        //Check customlog send from workstart,cargoswitch off on,workend,batteryRecord..

        if( DataLogRecorder.csvStringArray.size()> 0 )
        {
            String  csvString = DataLogRecorder.csvStringArray.pop();

            sdkTaskManagerObj.sendCSVDataToCustomLog(csvString);

        }
        else
        {

            //Get Current Battery Level From MainActivity

            String batteryLevel = BetteryManagersHelper.getBatteryLevel(context);

            beaconRecordModel.setBatteryLevel(batteryLevel);

            if(DataLogRecorder.isMonitorBatteryCharging())
            {
                beaconRecordModel.setBatteryStatus(DataLogRecorder.getBatteryStatus());

            }


            //Save data to local database
            manageCustomLog.beaconLogging();

            //1 Sec BeaconLogging Csv string send vai customlog
            sdkTaskManagerObj.sendCSVDataToCustomLog(beaconRecordModel.getCSV());


        }




        //get the current timeStamp for temp use for see log datetime
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        final String strDate = simpleDateFormat.format(calendar.getTime());


        if(GlobalConfig.isDebug)
        {
            Log.e(GlobalConfig.TAG,"ServiceRunning time = "+strDate);
        }


    }




    /**
     * starting
     */
    @Override
    public void onDestroy() {

        if(GlobalConfig.isDebug)
        {
            Log.e(GlobalConfig.TAG,"Service onDestroy ");
        }

        handler.removeCallbacks(timerTask);

        super.onDestroy();


    }




}