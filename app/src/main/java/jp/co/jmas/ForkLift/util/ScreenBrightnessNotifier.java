package jp.co.jmas.ForkLift.util;

import android.content.Context;
import android.os.PowerManager;
import android.util.Log;

import jp.co.jmas.ForkLift.classes.GlobalConfig;

/**
 * class for changing screen brightness
 */

public class ScreenBrightnessNotifier {

    // declaring objects
    PowerManager powerManager;
    PowerManager.WakeLock wakeLock;
    Context mContext;

    // Overloading constructor for context
    public ScreenBrightnessNotifier(Context mContext) {
        this.mContext = mContext;
        powerManager = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
    }

    /**
     * method for screen brightness wakelock
     */
    public void dim_brightness(boolean screen_brightness) {
        // checking the shared preference for the screen brightness dim switch value in setting
        if (screen_brightness) {

            // acquiring the wake lock
            wakeLock.acquire();

            if (GlobalConfig.isDebug) {
                Log.d(GlobalConfig.TAG,"wakeLock ="+String.valueOf(wakeLock.isHeld()));

            }

        }
        // checking the screen_brightness switch is false
        else {
            // if wakelock was held then it would release the wakelock
            if (wakeLock.isHeld()) {
                wakeLock.release();
            }
        }
    }
}
