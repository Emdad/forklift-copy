[33mcommit 50188ed6d12a29e3a080ded88394262d909c8db0[m
Merge: 4b929e5 2234d55
Author: Emdad Hossain <emdad.hossain321@gmail.com>
Date:   Mon Oct 3 16:22:01 2016 +0600

    Merge branch 'master' of https://bitbucket.org/banglafiremobile/forklift

[33mcommit 4b929e566356adc3074144080f34a4ebb9a1603a[m
Author: Emdad Hossain <emdad.hossain321@gmail.com>
Date:   Mon Oct 3 16:21:30 2016 +0600

    btn click event for input user number and select dept no in settings fragment

[33mcommit 2234d5580e75f5d611e788ecc707346d6ac41e91[m
Author: Arif Hossen <pg1@banglafire.com>
Date:   Mon Oct 3 16:00:23 2016 +0600

    Set Forkno as conditions wise

[33mcommit 2ef41540b8edcd570b63d033d65735951a731310[m
Merge: d054335 df6af9c
Author: Arif Hossen <pg1@banglafire.com>
Date:   Mon Oct 3 14:58:05 2016 +0600

    added new class skd

[33mcommit d054335a718fc2421b63f01a3357d62e7029396b[m
Author: Arif Hossen <pg1@banglafire.com>
Date:   Mon Oct 3 14:55:43 2016 +0600

    added new class skd

[33mcommit df6af9c55bbb237217834f934c71850bd8805a26[m
Author: Emdad Hossain <emdad.hossain321@gmail.com>
Date:   Mon Oct 3 14:52:37 2016 +0600

    list model abstraction

[33mcommit fb9755830f45cd5ea08b9f3ccc7bb55609648b80[m
Author: Emdad Hossain <emdad.hossain321@gmail.com>
Date:   Mon Oct 3 13:36:20 2016 +0600

    department no page design and add it to fragment

[33mcommit b0296fa6b0a86e98ce69ed75d2c398e31d3363fe[m
Merge: 560809e 6d83b7f
Author: Emdad Hossain <emdad.hossain321@gmail.com>
Date:   Mon Sep 5 15:23:07 2016 +0600

    Merge branch 'master' of https://bitbucket.org/banglafiremobile/forklift

[33mcommit 560809eff2094a3e1955f5dcee9d4c1a1335ff86[m
Author: Emdad Hossain <emdad.hossain321@gmail.com>
Date:   Mon Sep 5 15:22:53 2016 +0600

    user dept no

[33mcommit 6d83b7f34f1e8f7d019ee903a5b643467f4d9dde[m
Author: Arif Hossen <arifhossen2010@gmail.com>
Date:   Mon Sep 5 13:27:01 2016 +0600

    integrate qrcode reader

[33mcommit b1c79ecb2efc98f8e8bc2f204ea977bef497ed52[m
Author: Emdad Hossain <emdad.hossain321@gmail.com>
Date:   Mon Sep 5 09:47:56 2016 +0600

    backlight implemented

[33mcommit 301c9177f7290ea7d0d5f933124521b1bc66740a[m
Author: Arif Hossen <arifhossen2010@gmail.com>
Date:   Fri Sep 2 16:25:14 2016 +0600

    integrate qr code reader

[33mcommit 0dbad1f25efa8bc42937d5f91a8ca8c0347856bb[m
Author: Arif Hossen <arifhossen2010@gmail.com>
Date:   Thu Sep 1 15:44:29 2016 +0600

    added initial screen

[33mcommit 99694c914fbc7248b6bb076c94859a3e7a4ab35b[m
Author: Arif Hossen <arifhossen2010@gmail.com>
Date:   Thu Sep 1 15:13:39 2016 +0600

    added baseactivity

[33mcommit f6e788b4bf75a7df60feefefb7f422f3248dc80a[m
Merge: ba206b9 8ff9346
Author: Arif Hossen <arifhossen2010@gmail.com>
Date:   Thu Sep 1 15:07:37 2016 +0600

    delete old package

[33mcommit ba206b948f9d300896eb6afabdb67b7332ca93c5[m
Author: Arif Hossen <arifhossen2010@gmail.com>
Date:   Thu Sep 1 14:53:02 2016 +0600

    set settings alert and set alert of cargo status page end button

[33mcommit 8ff93469309830d6fb3d45b7aa781a4691b3445e[m
Merge: a4b6622 05ce256
Author: Emdad Hossain <emdad.hossain321@gmail.com>
Date:   Wed Aug 31 16:12:46 2016 +0600

    merge conflict resolved for settings write permission in maifest.xml

[33mcommit a4b662276aef404c949d0c916e9334891bdf1d9c[m
Author: Emdad Hossain <emdad.hossain321@gmail.com>
Date:   Wed Aug 31 16:09:59 2016 +0600

    done abstract class for getting the system brightness timer set remaining

[33mcommit 05ce2562e75014e3bd31a771fcbea48a66b45bc4[m
Author: Arif Hossen <arifhossen2010@gmail.com>
Date:   Wed Aug 31 15:23:41 2016 +0600

    added department select options

[33mcommit b10a7e7ffd425e9cfe8b08c499cc988435a9f89f[m
Author: Arif Hossen <arifhossen2010@gmail.com>
Date:   Wed Aug 31 13:00:49 2016 +0600

    added splash screen

[33mcommit 32ff0284b0a6e77f149011a31eb9e69ed24c62e9[m
Author: Arif Hossen <arifhossen2010@gmail.com>
Date:   Wed Aug 31 10:49:40 2016 +0600

    latest change dept

[33mcommit 5987cd4abf175f9079d3964e7f25d2d3369d5d5b[m
Merge: ed3f9d5 c3df155
Author: Arif Hossen <arifhossen2010@gmail.com>
Date:   Wed Aug 31 09:51:02 2016 +0600

    Merge branch 'master' of https://bitbucket.org/banglafiremobile/forklift

[33mcommit ed3f9d5b6838928cf4902ca61c25d3871ef6ca06[m
Author: Arif Hossen <arifhossen2010@gmail.com>
Date:   Wed Aug 31 09:50:36 2016 +0600

    added department no

[33mcommit c3df155361afddf5a2bef79b903f575b393425b8[m
Author: Emdad Hossain <emdad.hossain321@gmail.com>
Date:   Wed Aug 31 09:49:01 2016 +0600

    settings page fine tuning design for different screen sizes

[33mcommit 0cf45eff59e2ab945bb8741e2dce8103ba6cafa7[m
Author: Emdad Hossain <emdad.hossain321@gmail.com>
Date:   Wed Aug 31 08:47:02 2016 +0600

    settings  page design 30-08-16

[33mcommit afb0f8b88fd16ec0aa6deefb07a6ea2a5809c738[m
Merge: e9bdb2a 4f664f3
Author: Emdad Hossain <emdad.hossain321@gmail.com>
Date:   Tue Aug 30 16:15:11 2016 +0600

    Merge branch 'master' of https://bitbucket.org/banglafiremobile/forklift

[33mcommit 4f664f3a1b8853d0ac1714363bbfa72ca67bbd8e[m
Merge: 85189aa 9824262
Author: Arif Hossen <arifhossen2010@gmail.com>
Date:   Tue Aug 30 16:14:55 2016 +0600

    Merge branch 'master' of https://bitbucket.org/banglafiremobile/forklift

[33mcommit 85189aa39b82052fc1b51fbf391369310baec0a6[m
Author: Arif Hossen <arifhossen2010@gmail.com>
Date:   Tue Aug 30 16:12:52 2016 +0600

    ad

[33mcommit e9bdb2a30a799eb248514a762f048c2fb57a791b[m
Author: Emdad Hossain <emdad.hossain321@gmail.com>
Date:   Tue Aug 30 16:11:32 2016 +0600

    Settings page completed

[33mcommit 982426206a73768a2fd0135d3c3107cd41fe2947[m
Author: Emdad Hossain <emdad.hossain321@gmail.com>
Date:   Tue Aug 30 15:45:02 2016 +0600

    carousel fork selector image with large size

[33mcommit 4df5086339b7bbc8f44b037f66b4e9be8aeb0188[m
Merge: 9fcf71c 71fa321
Author: Emdad Hossain <emdad.hossain321@gmail.com>
Date:   Tue Aug 30 15:36:30 2016 +0600

    Merge branch 'master' of https://bitbucket.org/banglafiremobile/forklift

[33mcommit 71fa3217ee4784ee00171687e423032f22241459[m
Author: Arif Hossen <arifhossen2010@gmail.com>
Date:   Tue Aug 30 15:36:51 2016 +0600

    added switch

[33mcommit 9fcf71c05d841fb1cb646f0b1a56b738b0bd67cc[m
Author: Emdad Hossain <emdad.hossain321@gmail.com>
Date:   Tue Aug 30 15:36:18 2016 +0600

    settings page desgin complete

[33mcommit 7bf1aa64bb05af91d5bd6518d00eb35630b19835[m
Merge: 23b5b07 1bba463
Author: Emdad Hossain <emdad.hossain321@gmail.com>
Date:   Tue Aug 30 13:49:38 2016 +0600

    settings page design

[33mcommit 1bba463f37629550b0f26923052c4a223902dc68[m
Author: Arif Hossen <arifhossen2010@gmail.com>
Date:   Tue Aug 30 13:40:15 2016 +0600

    commit code structure

[33mcommit 23b5b07b5b3a92643ae5323b66af714568dad307[m
Author: Emdad Hossain <emdad.hossain321@gmail.com>
Date:   Tue Aug 30 13:40:08 2016 +0600

    settings page design

[33mcommit 7213d4f8a7c5cec40f43002146d400f4e6533540[m
Author: Arif Hossen <arifhossen2010@gmail.com>
Date:   Tue Aug 30 10:54:50 2016 +0600

    add settings

[33mcommit 43382082a9e3194b327c15ff370587e377298168[m
Author: Arif Hossen <arifhossen2010@gmail.com>
Date:   Tue Aug 30 10:47:45 2016 +0600

    initial commit
